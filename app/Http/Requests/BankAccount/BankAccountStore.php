<?php

namespace App\Http\Requests\BankAccount;

class BankAccountStore extends BankAccountDoc
{
    public function authorize() {
        return auth()->user()->tokenCan('create');
    }

    public function rules()
    {
        return [
            'bank_name'      => ['required', 'max:255'],
            'agency_number'  => ['required', 'max:10'],
            'account_number' => ['required', 'max:10'],
            'balance_start'  => ['required', 'numeric'],
        ];
    }
}
