<?php

namespace App\Http\Requests\Provider;

class ProviderUpdate extends ProviderDoc
{
    public function authorize()
    {
        return auth()->user()->tokenCan('update');
    }

    public function rules()
    {
        return [
            "name"    => ['filled', 'max:255'],
            "phone"   => ['filled', 'max:255'],
            "address" => ['filled', 'max:20'],
            "email"   => ['filled', 'email', 'max:20'],
        ];
    }
}
