<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Laravel Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/style.css") }}" media="screen" />
        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/print.css") }}" media="print" />
        <script src="{{ asset("vendor/scribe/js/all.js") }}"></script>

        <link rel="stylesheet" href="{{ asset("vendor/scribe/css/highlight-darcula.css") }}" media="" />
        <script src="{{ asset("vendor/scribe/js/highlight.pack.js") }}"></script>
    <script>hljs.initHighlightingOnLoad();</script>

</head>

<body class="" data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">
<a href="#" id="nav-button">
      <span>
        NAV
            <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="-image" class=""/>
      </span>
</a>
<div class="tocify-wrapper">
                <div class="lang-selector">
                            <a href="#" data-language-name="bash">bash</a>
                            <a href="#" data-language-name="javascript">javascript</a>
                    </div>
        <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>

    <ul id="toc">
    </ul>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI (Swagger) spec</a></li>
                            <li><a href='http://github.com/knuckleswtf/scribe'>Documentation powered by Scribe ✍</a></li>
                    </ul>
            <ul class="toc-footer" id="last-updated">
            <li>Last updated: May 15 2021</li>
        </ul>
</div>
<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1>Introduction</h1>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
<script>
    var baseUrl = "http://127.0.0.1:8000";
</script>
<script src="{{ asset("vendor/scribe/js/tryitout-2.5.3.js") }}"></script>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">http://127.0.0.1:8000</code></pre><h1>Authenticating requests</h1>
<p>Authenticate requests to this API's endpoints by sending an <strong><code>Authorization</code></strong> header with the value <strong><code>"Bearer {YOUR_AUTH_KEY}"</code></strong>.</p>
<p>All authenticated endpoints are marked with a <code>requires authentication</code> badge in the documentation below.</p>
<p>You can retrieve your token by visiting your dashboard and clicking <b>Generate API token</b>.</p><h1>Conta a Pagar Liquidar/Estornar</h1>
<h2>Liquidar conta a pagar</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Realiza a liquidação de uma conta a pagar</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://127.0.0.1:8000/api/account-debts/pay/9" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/pay/9"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Debt] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-account-debts-pay--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-account-debts-pay--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-account-debts-pay--account_debt-"></code></pre>
</div>
<div id="execution-error-PUTapi-account-debts-pay--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-account-debts-pay--account_debt-"></code></pre>
</div>
<form id="form-PUTapi-account-debts-pay--account_debt-" data-method="PUT" data-path="api/account-debts/pay/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-account-debts-pay--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-account-debts-pay--account_debt-" onclick="tryItOut('PUTapi-account-debts-pay--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-account-debts-pay--account_debt-" onclick="cancelTryOut('PUTapi-account-debts-pay--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-account-debts-pay--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/account-debts/pay/{account_debt}</code></b>
</p>
<p>
<label id="auth-PUTapi-account-debts-pay--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-account-debts-pay--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="PUTapi-account-debts-pay--account_debt-" data-component="url" required  hidden>
<br>
O id da conta a pagar
</p>
</form>
<h2>Estornar valor da transação</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Realiza o estorno de um valor pago</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://127.0.0.1:8000/api/account-debts/refund/20" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/refund/20"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Debt] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-account-debts-refund--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-account-debts-refund--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-account-debts-refund--account_debt-"></code></pre>
</div>
<div id="execution-error-PUTapi-account-debts-refund--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-account-debts-refund--account_debt-"></code></pre>
</div>
<form id="form-PUTapi-account-debts-refund--account_debt-" data-method="PUT" data-path="api/account-debts/refund/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-account-debts-refund--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-account-debts-refund--account_debt-" onclick="tryItOut('PUTapi-account-debts-refund--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-account-debts-refund--account_debt-" onclick="cancelTryOut('PUTapi-account-debts-refund--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-account-debts-refund--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/account-debts/refund/{account_debt}</code></b>
</p>
<p>
<label id="auth-PUTapi-account-debts-refund--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-account-debts-refund--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="PUTapi-account-debts-refund--account_debt-" data-component="url" required  hidden>
<br>
O id da conta a pagar
</p>
</form><h1>Conta Bancaria</h1>
<h2>Listar Contas Bancarias</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retonar todos os registros do banco</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/bank-account" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/bank-account"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": [
        {
            "account_id": 1,
            "bank_name": "BB",
            "agency_number": "BRA013",
            "account_number": "39401039",
            "balance_start": 200.16,
            "created_at": "2021-05-03T17:50:16.000000Z",
            "updated_at": "2021-05-03T19:45:28.000000Z"
        },
        {
            "account_id": 2,
            "bank_name": "Kunze, Kilback and Weber",
            "agency_number": "28845381",
            "account_number": "2003427881",
            "balance_start": 154491.26,
            "created_at": "2021-05-03T17:50:16.000000Z",
            "updated_at": "2021-05-03T17:50:16.000000Z"
        }
    ],
    "links": {
        "first": "http:\/\/127.0.0.1:8000\/api\/bank-account?page=1",
        "last": "http:\/\/127.0.0.1:8000\/api\/bank-account?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&amp;laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/bank-account?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Próximo &amp;raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/127.0.0.1:8000\/api\/bank-account",
        "per_page": 15,
        "to": 13,
        "total": 13
    }
}</code></pre>
<div id="execution-results-GETapi-bank-account" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-bank-account"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-bank-account"></code></pre>
</div>
<div id="execution-error-GETapi-bank-account" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-bank-account"></code></pre>
</div>
<form id="form-GETapi-bank-account" data-method="GET" data-path="api/bank-account" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-bank-account', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-bank-account" onclick="tryItOut('GETapi-bank-account');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-bank-account" onclick="cancelTryOut('GETapi-bank-account');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-bank-account" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/bank-account</code></b>
</p>
<p>
<label id="auth-GETapi-bank-account" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-bank-account" data-component="header"></label>
</p>
</form>
<h2>Criar Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Cria uma nova conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://127.0.0.1:8000/api/bank-account" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"bank_name":"ITAU","agency_number":"BRA01","account_number":"2003427881","balance_start":154491.26}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/bank-account"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "bank_name": "ITAU",
    "agency_number": "BRA01",
    "account_number": "2003427881",
    "balance_start": 154491.26
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (201):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "account_id": 1,
        "bank_name": "BB",
        "agency_number": "BRA013",
        "account_number": "39401039",
        "balance_start": 200.16,
        "created_at": "2021-05-03T17:50:16.000000Z",
        "updated_at": "2021-05-03T19:45:28.000000Z"
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "bank_name": [
            "O campo bank_name é obrigatório.",
            "O campo bank_name não pode ser superior a 255 caracteres."
        ],
        "agency_number": [
            "O campo agency_number é obrigatório.",
            "O campo agency_number não pode ser superior a 10 caracteres."
        ],
        "account_number": [
            "O campo account_number é obrigatório.",
            "O campo account_number não pode ser superior a 10 caracteres."
        ],
        "balance_start": [
            "O campo balance_start é obrigatório.",
            "O campo balance_start deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-POSTapi-bank-account" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-bank-account"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-bank-account"></code></pre>
</div>
<div id="execution-error-POSTapi-bank-account" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-bank-account"></code></pre>
</div>
<form id="form-POSTapi-bank-account" data-method="POST" data-path="api/bank-account" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-bank-account', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-bank-account" onclick="tryItOut('POSTapi-bank-account');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-bank-account" onclick="cancelTryOut('POSTapi-bank-account');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-bank-account" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/bank-account</code></b>
</p>
<p>
<label id="auth-POSTapi-bank-account" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-bank-account" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>bank_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="bank_name" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Nome do Banco.
</p>
<p>
<b><code>agency_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="agency_number" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Agencia bancaria.
</p>
<p>
<b><code>account_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="account_number" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Numero da conta bancaria.
</p>
<p>
<b><code>balance_start</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="balance_start" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Balanço da conta bancaria.
</p>

</form>
<h2>Detalhar Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna os dados da conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/bank-account/16" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/bank-account/16"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\BankAccount] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "account_id": 1,
        "bank_name": "BB",
        "agency_number": "BRA013",
        "account_number": "39401039",
        "balance_start": 200.16,
        "created_at": "2021-05-03T17:50:16.000000Z",
        "updated_at": "2021-05-03T19:45:28.000000Z"
    }
}</code></pre>
<div id="execution-results-GETapi-bank-account--bank_account-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-bank-account--bank_account-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-bank-account--bank_account-"></code></pre>
</div>
<div id="execution-error-GETapi-bank-account--bank_account-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-bank-account--bank_account-"></code></pre>
</div>
<form id="form-GETapi-bank-account--bank_account-" data-method="GET" data-path="api/bank-account/{bank_account}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-bank-account--bank_account-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-bank-account--bank_account-" onclick="tryItOut('GETapi-bank-account--bank_account-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-bank-account--bank_account-" onclick="cancelTryOut('GETapi-bank-account--bank_account-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-bank-account--bank_account-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<label id="auth-GETapi-bank-account--bank_account-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-bank-account--bank_account-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>bank_account</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="bank_account" data-endpoint="GETapi-bank-account--bank_account-" data-component="url" required  hidden>
<br>
O valor do id da conta bancaria
</p>
</form>
<h2>Atualizar Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Atualiza os dados da conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://127.0.0.1:8000/api/bank-account/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"bank_name":"ITAU","agency_number":"BRA01","account_number":"2003427881","balance_start":154491.26}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/bank-account/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "bank_name": "ITAU",
    "agency_number": "BRA01",
    "account_number": "2003427881",
    "balance_start": 154491.26
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\BankAccount] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "account_id": 1,
        "bank_name": "BB",
        "agency_number": "BRA013",
        "account_number": "39401039",
        "balance_start": 200.16,
        "created_at": "2021-05-03T17:50:16.000000Z",
        "updated_at": "2021-05-03T19:45:28.000000Z"
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "bank_name": [
            "O campo bank_name não pode ser superior a 255 caracteres.",
            "O campo bank_name deve ter um valor."
        ],
        "agency_number": [
            "O campo agency_number não pode ser superior a 10 caracteres.",
            "O campo agency_number deve ter um valor."
        ],
        "account_number": [
            "O campo account_number não pode ser superior a 10 caracteres.",
            "O campo account_number deve ter um valor."
        ],
        "balance_start": [
            "O campo balance_start deve ser um número.",
            "O campo balance_start deve ter um valor."
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-bank-account--bank_account-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-bank-account--bank_account-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-bank-account--bank_account-"></code></pre>
</div>
<div id="execution-error-PUTapi-bank-account--bank_account-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-bank-account--bank_account-"></code></pre>
</div>
<form id="form-PUTapi-bank-account--bank_account-" data-method="PUT" data-path="api/bank-account/{bank_account}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-bank-account--bank_account-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-bank-account--bank_account-" onclick="tryItOut('PUTapi-bank-account--bank_account-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-bank-account--bank_account-" onclick="cancelTryOut('PUTapi-bank-account--bank_account-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-bank-account--bank_account-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<label id="auth-PUTapi-bank-account--bank_account-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-bank-account--bank_account-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>bank_account</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="bank_account" data-endpoint="PUTapi-bank-account--bank_account-" data-component="url" required  hidden>
<br>
O valor do id da conta bancaria
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>bank_name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="bank_name" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Nome do Banco.
</p>
<p>
<b><code>agency_number</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="agency_number" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Agencia bancaria.
</p>
<p>
<b><code>account_number</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="account_number" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Numero da conta bancaria.
</p>
<p>
<b><code>balance_start</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="balance_start" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Balanço da conta bancaria.
</p>

</form>
<h2>Excluir Conta Bancaria</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Exclui uma conta bancaria</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://127.0.0.1:8000/api/bank-account/11" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/bank-account/11"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\BankAccount] 3"
}</code></pre>
<div id="execution-results-DELETEapi-bank-account--bank_account-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-bank-account--bank_account-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-bank-account--bank_account-"></code></pre>
</div>
<div id="execution-error-DELETEapi-bank-account--bank_account-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-bank-account--bank_account-"></code></pre>
</div>
<form id="form-DELETEapi-bank-account--bank_account-" data-method="DELETE" data-path="api/bank-account/{bank_account}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-bank-account--bank_account-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-bank-account--bank_account-" onclick="tryItOut('DELETEapi-bank-account--bank_account-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-bank-account--bank_account-" onclick="cancelTryOut('DELETEapi-bank-account--bank_account-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-bank-account--bank_account-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<label id="auth-DELETEapi-bank-account--bank_account-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-bank-account--bank_account-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>bank_account</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="bank_account" data-endpoint="DELETEapi-bank-account--bank_account-" data-component="url" required  hidden>
<br>
O valor do id da conta bancaria
</p>
</form><h1>Contas Pedentes</h1>
<h2>Listar todos as contas pendentes</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retonar todas contas pendentes</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": [
        {
            "debt_id": 1,
            "provider_id": 1,
            "description": "teste teste teste",
            "value": 10.1,
            "payed_at": null,
            "created_at": "2021-05-08T14:43:35.000000Z",
            "updated_at": "2021-05-13T17:48:45.000000Z",
            "history": [
                {
                    "history_id": 1,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": true,
                    "created_at": "2021-05-13T16:48:51.000000Z",
                    "updated_at": "2021-05-13T16:48:51.000000Z"
                },
                {
                    "history_id": 2,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": true,
                    "created_at": "2021-05-13T16:50:52.000000Z",
                    "updated_at": "2021-05-13T16:50:52.000000Z"
                },
                {
                    "history_id": 3,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": false,
                    "created_at": "2021-05-13T16:58:41.000000Z",
                    "updated_at": "2021-05-13T16:58:41.000000Z"
                }
            ]
        },
        {
            "debt_id": 3,
            "provider_id": 1,
            "description": "teste teste teste",
            "value": 10.1,
            "payed_at": null,
            "created_at": "2021-05-10T22:38:08.000000Z",
            "updated_at": "2021-05-10T22:38:08.000000Z",
            "history": []
        },
        {
            "debt_id": 11,
            "provider_id": 72,
            "description": "teste teste teste",
            "value": 4012.12,
            "payed_at": null,
            "created_at": "2021-05-13T13:06:44.000000Z",
            "updated_at": "2021-05-13T17:53:32.000000Z",
            "history": [
                {
                    "history_id": 6,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": true,
                    "created_at": "2021-05-13T17:21:11.000000Z",
                    "updated_at": "2021-05-13T17:21:11.000000Z"
                },
                {
                    "history_id": 12,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": false,
                    "created_at": "2021-05-13T17:53:32.000000Z",
                    "updated_at": "2021-05-13T17:53:32.000000Z"
                }
            ]
        }
    ],
    "links": {
        "first": "http:\/\/127.0.0.1:8000\/api\/account-debts?page=1",
        "last": "http:\/\/127.0.0.1:8000\/api\/account-debts?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&amp;laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/account-debts?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Próximo &amp;raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/127.0.0.1:8000\/api\/account-debts",
        "per_page": 15,
        "to": 10,
        "total": 10
    }
}</code></pre>
<div id="execution-results-GETapi-account-debts" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts"></code></pre>
</div>
<form id="form-GETapi-account-debts" data-method="GET" data-path="api/account-debts" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts" onclick="tryItOut('GETapi-account-debts');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts" onclick="cancelTryOut('GETapi-account-debts');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts" data-component="header"></label>
</p>
</form>
<h2>Criar conta pendente</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Cria uma nova conta pendente</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://127.0.0.1:8000/api/account-debts" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"provider_id":1234,"description":"CONTA DE LUZ","value":10101.12}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "provider_id": 1234,
    "description": "CONTA DE LUZ",
    "value": 10101.12
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (201):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "provider_id": [
            "O campo provider_id é obrigatório.",
            "O campo provider_id deve ser um número inteiro.",
            "O campo provider_id selecionado é inválido."
        ],
        "description": [
            "O campo description é obrigatório.",
            "O campo description não pode ser superior a 255 caracteres."
        ],
        "value": [
            "O campo value é obrigatório.",
            "O campo value deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-POSTapi-account-debts" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-account-debts"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-account-debts"></code></pre>
</div>
<div id="execution-error-POSTapi-account-debts" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-account-debts"></code></pre>
</div>
<form id="form-POSTapi-account-debts" data-method="POST" data-path="api/account-debts" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-account-debts', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-account-debts" onclick="tryItOut('POSTapi-account-debts');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-account-debts" onclick="cancelTryOut('POSTapi-account-debts');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-account-debts" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/account-debts</code></b>
</p>
<p>
<label id="auth-POSTapi-account-debts" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-account-debts" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>provider_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider_id" data-endpoint="POSTapi-account-debts" data-component="body" required  hidden>
<br>
id do fornecedor ao qual se tem a divida.
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="description" data-endpoint="POSTapi-account-debts" data-component="body" required  hidden>
<br>
Descrição da divida.
</p>
<p>
<b><code>value</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="value" data-endpoint="POSTapi-account-debts" data-component="body" required  hidden>
<br>
Valor da divida.
</p>

</form>
<h2>Detalhar conta pendente</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna os dados da conta pendente</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts/17" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/17"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Debt] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-GETapi-account-debts--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts--account_debt-"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts--account_debt-"></code></pre>
</div>
<form id="form-GETapi-account-debts--account_debt-" data-method="GET" data-path="api/account-debts/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts--account_debt-" onclick="tryItOut('GETapi-account-debts--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts--account_debt-" onclick="cancelTryOut('GETapi-account-debts--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="GETapi-account-debts--account_debt-" data-component="url" required  hidden>
<br>
O id do debito
</p>
</form>
<h2>Atualizar conta pendente</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Atualiza os dados da conta pendente</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://127.0.0.1:8000/api/account-debts/18" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"provider_id":1234,"description":"CONTA DE LUZ","value":10101.12}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/18"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "provider_id": 1234,
    "description": "CONTA DE LUZ",
    "value": 10101.12
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Debt] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "provider_id": [
            "O campo provider_id deve ter um valor.",
            "O campo provider_id deve ser um número inteiro.",
            "O campo provider_id selecionado é inválido."
        ],
        "description": [
            "O campo description deve ter um valor.",
            "O campo description não pode ser superior a 255 caracteres."
        ],
        "value": [
            "O campo value deve ter um valor.",
            "O campo value deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-account-debts--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-account-debts--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-account-debts--account_debt-"></code></pre>
</div>
<div id="execution-error-PUTapi-account-debts--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-account-debts--account_debt-"></code></pre>
</div>
<form id="form-PUTapi-account-debts--account_debt-" data-method="PUT" data-path="api/account-debts/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-account-debts--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-account-debts--account_debt-" onclick="tryItOut('PUTapi-account-debts--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-account-debts--account_debt-" onclick="cancelTryOut('PUTapi-account-debts--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-account-debts--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<label id="auth-PUTapi-account-debts--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-account-debts--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="PUTapi-account-debts--account_debt-" data-component="url" required  hidden>
<br>
O id do debito
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>provider_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="provider_id" data-endpoint="PUTapi-account-debts--account_debt-" data-component="body"  hidden>
<br>
id do fornecedor ao qual se tem a divida.
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="description" data-endpoint="PUTapi-account-debts--account_debt-" data-component="body"  hidden>
<br>
Descrição da divida.
</p>
<p>
<b><code>value</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="value" data-endpoint="PUTapi-account-debts--account_debt-" data-component="body"  hidden>
<br>
Valor da divida.
</p>

</form>
<h2>Excluir conta pendente</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Exclui uma conta pendente</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://127.0.0.1:8000/api/account-debts/8" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/8"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Debt] 3"
}</code></pre>
<div id="execution-results-DELETEapi-account-debts--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-account-debts--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-account-debts--account_debt-"></code></pre>
</div>
<div id="execution-error-DELETEapi-account-debts--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-account-debts--account_debt-"></code></pre>
</div>
<form id="form-DELETEapi-account-debts--account_debt-" data-method="DELETE" data-path="api/account-debts/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-account-debts--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-account-debts--account_debt-" onclick="tryItOut('DELETEapi-account-debts--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-account-debts--account_debt-" onclick="cancelTryOut('DELETEapi-account-debts--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-account-debts--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<label id="auth-DELETEapi-account-debts--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-account-debts--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="DELETEapi-account-debts--account_debt-" data-component="url" required  hidden>
<br>
O id do debito
</p>
</form><h1>Endpoints</h1>
<h2>Listagem de usuarios</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna todos os usuarios cadastrados</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/user" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/user"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">[
    {
        "id": 1,
        "name": "Rosalyn Winters",
        "email": "lycidoryv@mailinator.com",
        "created_at": "2021-04-30T21:54:08.000000Z",
        "updated_at": "2021-04-30T21:54:08.000000Z"
    },
    {
        "id": 2,
        "name": "Georgette Krajcik",
        "email": "maximo.moen@example.com",
        "created_at": "2021-04-30T22:54:46.000000Z",
        "updated_at": "2021-04-30T22:54:46.000000Z"
    }
]</code></pre>
<div id="execution-results-GETapi-user" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-user"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-user"></code></pre>
</div>
<div id="execution-error-GETapi-user" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-user"></code></pre>
</div>
<form id="form-GETapi-user" data-method="GET" data-path="api/user" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-user', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-user" onclick="tryItOut('GETapi-user');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-user" onclick="cancelTryOut('GETapi-user');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-user" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/user</code></b>
</p>
<p>
<label id="auth-GETapi-user" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-user" data-component="header"></label>
</p>
</form><h1>Fornecedor</h1>
<h2>Listar Fornecedores</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retonar todos os registros do banco</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/provider" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/provider"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": [
        {
            "id": 1,
            "name": "Nome Atualizado 2",
            "phone": "(469) 672-3724",
            "address": "Novo Endereço",
            "email": "harry19@example.com",
            "created_at": "2021-05-01T19:24:55.000000Z",
            "updated_at": "2021-05-01T20:28:19.000000Z",
            "debts": [
                {
                    "debt_id": 1,
                    "provider_id": 1,
                    "description": "teste teste teste",
                    "value": 10.1,
                    "payed_at": null,
                    "created_at": "2021-05-08T14:43:35.000000Z",
                    "updated_at": "2021-05-09T15:03:36.000000Z"
                },
                {
                    "debt_id": 3,
                    "provider_id": 1,
                    "description": "teste teste teste",
                    "value": 10.1,
                    "payed_at": null,
                    "created_at": "2021-05-10T22:38:08.000000Z",
                    "updated_at": "2021-05-10T22:38:08.000000Z"
                }
            ]
        },
        {
            "id": 2,
            "name": "Mr. Leland Turcotte MD",
            "phone": "+18656291273",
            "address": "1225 Dewitt Stravenue Apt. 158\nEast Dexterview, NV 61312",
            "email": "vivian.bartoletti@example.org",
            "created_at": "2021-05-01T19:24:55.000000Z",
            "updated_at": "2021-05-01T19:24:55.000000Z",
            "debts": []
        },
        {
            "id": 5,
            "name": "Rose Graham",
            "phone": "+1 (743) 965-6878",
            "address": "22762 Kristy Pass Apt. 661\nPort Dean, KS 84062-8616",
            "email": "tschumm@example.net",
            "created_at": "2021-05-01T19:24:56.000000Z",
            "updated_at": "2021-05-01T19:24:56.000000Z",
            "debts": []
        }
    ],
    "links": {
        "first": "http:\/\/127.0.0.1:8000\/api\/provider?page=1",
        "last": "http:\/\/127.0.0.1:8000\/api\/provider?page=5",
        "prev": null,
        "next": "http:\/\/127.0.0.1:8000\/api\/provider?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 5,
        "links": [
            {
                "url": null,
                "label": "&amp;laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=3",
                "label": "3",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=4",
                "label": "4",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=5",
                "label": "5",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=2",
                "label": "Próximo &amp;raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/127.0.0.1:8000\/api\/provider",
        "per_page": 15,
        "to": 15,
        "total": 63
    }
}</code></pre>
<div id="execution-results-GETapi-provider" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-provider"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-provider"></code></pre>
</div>
<div id="execution-error-GETapi-provider" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-provider"></code></pre>
</div>
<form id="form-GETapi-provider" data-method="GET" data-path="api/provider" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-provider', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-provider" onclick="tryItOut('GETapi-provider');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-provider" onclick="cancelTryOut('GETapi-provider');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-provider" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/provider</code></b>
</p>
<p>
<label id="auth-GETapi-provider" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-provider" data-component="header"></label>
</p>
</form>
<h2>Criar fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Cria um novo fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://127.0.0.1:8000/api/provider" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"FORNECEDOR DE ENERGIA","address":"Avenida Brasil, 200, S\u00e3o Paulo","phone":"(38) 4002-8922","email":"energiaecia@fornecedor.com","debts":[{"description":"CONTA DE ENERGIA","value":99.67}]}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/provider"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "FORNECEDOR DE ENERGIA",
    "address": "Avenida Brasil, 200, S\u00e3o Paulo",
    "phone": "(38) 4002-8922",
    "email": "energiaecia@fornecedor.com",
    "debts": [
        {
            "description": "CONTA DE ENERGIA",
            "value": 99.67
        }
    ]
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (201):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "id": 1,
        "name": "Nome Atualizado 2",
        "phone": "(469) 672-3724",
        "address": "Novo Endereço",
        "email": "harry19@example.com",
        "created_at": "2021-05-01T19:24:55.000000Z",
        "updated_at": "2021-05-01T20:28:19.000000Z",
        "debts": [
            {
                "debt_id": 1,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-08T14:43:35.000000Z",
                "updated_at": "2021-05-09T15:03:36.000000Z"
            },
            {
                "debt_id": 3,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-10T22:38:08.000000Z",
                "updated_at": "2021-05-10T22:38:08.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "O campo name é obrigatório.",
            "O campo name não pode ser superior a 255 caracteres."
        ],
        "address": [
            "O campo address é obrigatório.",
            "O campo address não pode ser superior a 255 caracteres."
        ],
        "email": [
            "O campo email deve ser um endereço de e-mail válido.",
            "O campo email não pode ser superior a 20 caracteres."
        ],
        "phone": [
            "O campo phone não pode ser superior a 20 caracteres."
        ],
        "debts": [
            "O campo debts deve ter um valor.",
            "O campo debts deve ser uma matriz."
        ],
        "debts.0.description": [
            "O campo debts.0.description é obrigatório.",
            "O campo debts.0.description não pode ser superior a 255 caracteres."
        ],
        "debts.0.value": [
            "O campo debts.0.value é obrigatório.",
            "O campo debts.0.value deve ser um número."
        ]
    }
}</code></pre>
<div id="execution-results-POSTapi-provider" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-provider"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-provider"></code></pre>
</div>
<div id="execution-error-POSTapi-provider" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-provider"></code></pre>
</div>
<form id="form-POSTapi-provider" data-method="POST" data-path="api/provider" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-provider', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-provider" onclick="tryItOut('POSTapi-provider');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-provider" onclick="cancelTryOut('POSTapi-provider');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-provider" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/provider</code></b>
</p>
<p>
<label id="auth-POSTapi-provider" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-provider" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="address" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="phone" data-endpoint="POSTapi-provider" data-component="body"  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-provider" data-component="body"  hidden>
<br>
O email do fornecedor. O campo value deve ser um endereço de e-mail válido.
</p>
<p>
<details>
<summary>
<b><code>debts</code></b>&nbsp;&nbsp;<small>object[]</small>     <i>optional</i> &nbsp;
<br>

</summary>
<br>
<p>
<b><code>debts[].description</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="debts.0.description" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
A descrição da conta a pagar.
</p>
<p>
<b><code>debts[].value</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="debts.0.value" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
O valor da conta a pagar.
</p>
</details>
</p>

</form>
<h2>Detalhar Fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna os dados do fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/provider/4" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/provider/4"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Provider] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "id": 1,
        "name": "Nome Atualizado 2",
        "phone": "(469) 672-3724",
        "address": "Novo Endereço",
        "email": "harry19@example.com",
        "created_at": "2021-05-01T19:24:55.000000Z",
        "updated_at": "2021-05-01T20:28:19.000000Z",
        "debts": [
            {
                "debt_id": 1,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-08T14:43:35.000000Z",
                "updated_at": "2021-05-09T15:03:36.000000Z"
            },
            {
                "debt_id": 3,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-10T22:38:08.000000Z",
                "updated_at": "2021-05-10T22:38:08.000000Z"
            }
        ]
    }
}</code></pre>
<div id="execution-results-GETapi-provider--provider-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-provider--provider-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-provider--provider-"></code></pre>
</div>
<div id="execution-error-GETapi-provider--provider-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-provider--provider-"></code></pre>
</div>
<form id="form-GETapi-provider--provider-" data-method="GET" data-path="api/provider/{provider}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-provider--provider-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-provider--provider-" onclick="tryItOut('GETapi-provider--provider-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-provider--provider-" onclick="cancelTryOut('GETapi-provider--provider-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-provider--provider-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<label id="auth-GETapi-provider--provider-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-provider--provider-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>provider</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider" data-endpoint="GETapi-provider--provider-" data-component="url" required  hidden>
<br>
O id do fornecedor
</p>
</form>
<h2>Atualizar Fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Atualiza os dados do fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X PUT \
    "http://127.0.0.1:8000/api/provider/4" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"FORNECEDOR DE ENERGIA","phone":"(38) 4002-8922","address":"Avenida Brasil, 200, S\u00e3o Paulo","email":"energiaecia@fornecedor.com"}'
</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/provider/4"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "FORNECEDOR DE ENERGIA",
    "phone": "(38) 4002-8922",
    "address": "Avenida Brasil, 200, S\u00e3o Paulo",
    "email": "energiaecia@fornecedor.com"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Provider] 3"
}</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">{
    "data": {
        "id": 1,
        "name": "Nome Atualizado 2",
        "phone": "(469) 672-3724",
        "address": "Novo Endereço",
        "email": "harry19@example.com",
        "created_at": "2021-05-01T19:24:55.000000Z",
        "updated_at": "2021-05-01T20:28:19.000000Z",
        "debts": [
            {
                "debt_id": 1,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-08T14:43:35.000000Z",
                "updated_at": "2021-05-09T15:03:36.000000Z"
            },
            {
                "debt_id": 3,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-10T22:38:08.000000Z",
                "updated_at": "2021-05-10T22:38:08.000000Z"
            }
        ]
    }
}</code></pre>
<blockquote>
<p>Example response (422):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "O campo name não pode ser superior a 255 caracteres.",
            "O campo name deve ter um valor."
        ],
        "address": [
            "O campo address não pode ser superior a 255 caracteres.",
            "O campo address deve ter um valor."
        ],
        "email": [
            "O campo email deve ser um endereço de e-mail válido.",
            "O campo email não pode ser superior a 20 caracteres.",
            "O campo email deve ter um valor."
        ],
        "phone": [
            "O campo phone não pode ser superior a 20 caracteres.",
            "O campo phone deve ter um valor."
        ]
    }
}</code></pre>
<div id="execution-results-PUTapi-provider--provider-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-provider--provider-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-provider--provider-"></code></pre>
</div>
<div id="execution-error-PUTapi-provider--provider-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-provider--provider-"></code></pre>
</div>
<form id="form-PUTapi-provider--provider-" data-method="PUT" data-path="api/provider/{provider}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-provider--provider-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-provider--provider-" onclick="tryItOut('PUTapi-provider--provider-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-provider--provider-" onclick="cancelTryOut('PUTapi-provider--provider-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-provider--provider-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<label id="auth-PUTapi-provider--provider-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-provider--provider-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>provider</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider" data-endpoint="PUTapi-provider--provider-" data-component="url" required  hidden>
<br>
O id do fornecedor
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="name" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="phone" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="address" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O email do fornecedor. O campo value deve ser um endereço de e-mail válido.
</p>

</form>
<h2>Excluir Fornecedor</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Exclui um fornecedor</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X DELETE \
    "http://127.0.0.1:8000/api/provider/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/provider/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (404):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "No query results for model [App\\Models\\Provider] 3"
}</code></pre>
<div id="execution-results-DELETEapi-provider--provider-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-provider--provider-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-provider--provider-"></code></pre>
</div>
<div id="execution-error-DELETEapi-provider--provider-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-provider--provider-"></code></pre>
</div>
<form id="form-DELETEapi-provider--provider-" data-method="DELETE" data-path="api/provider/{provider}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-provider--provider-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-provider--provider-" onclick="tryItOut('DELETEapi-provider--provider-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-provider--provider-" onclick="cancelTryOut('DELETEapi-provider--provider-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-provider--provider-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<label id="auth-DELETEapi-provider--provider-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-provider--provider-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>provider</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider" data-endpoint="DELETEapi-provider--provider-" data-component="url" required  hidden>
<br>
O id do fornecedor
</p>
</form><h1>Relatorio</h1>
<h2>Contas pendentes</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna a listagem de contas pendentes de pagamento. Utilize header Accept application/json</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts/report/pending" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/report/pending"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">[
    {
        "id": 3,
        "provider_id": 1,
        "description": "Conta de luz",
        "value": 10.1,
        "payed_at": null,
        "created_at": "2021-05-10T22:38:08.000000Z",
        "updated_at": "2021-05-15T16:27:24.000000Z",
        "deleted_at": null
    },
    {
        "id": 5,
        "provider_id": 1,
        "description": "Conta de agua",
        "value": 10.1,
        "payed_at": null,
        "created_at": "2021-05-10T22:39:02.000000Z",
        "updated_at": "2021-05-15T16:27:32.000000Z",
        "deleted_at": null
    },
    {
        "id": 8,
        "provider_id": 1,
        "description": "Conta de internet",
        "value": 123.21,
        "payed_at": null,
        "created_at": "2021-05-10T22:40:45.000000Z",
        "updated_at": "2021-05-15T16:27:35.000000Z",
        "deleted_at": null
    }
]</code></pre>
<div id="execution-results-GETapi-account-debts-report-pending" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts-report-pending"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts-report-pending"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts-report-pending" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts-report-pending"></code></pre>
</div>
<form id="form-GETapi-account-debts-report-pending" data-method="GET" data-path="api/account-debts/report/pending" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts-report-pending', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts-report-pending" onclick="tryItOut('GETapi-account-debts-report-pending');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts-report-pending" onclick="cancelTryOut('GETapi-account-debts-report-pending');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts-report-pending" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts/report/pending</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts-report-pending" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts-report-pending" data-component="header"></label>
</p>
</form>
<h2>Contas pagas</h2>
<p><small class="badge badge-darkred">requires authentication</small></p>
<p>Retorna a listagem de contas pagas. Utilize header Accept application/json</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts/report/payed" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/report/payed"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre>
<blockquote>
<p>Example response (200):</p>
</blockquote>
<pre><code class="language-json">[
    {
        "id": 3,
        "provider_id": 1,
        "description": "Conta de luz",
        "value": 10.1,
        "payed_at": "2021-05-15T16:27:24.000000Z",
        "created_at": "2021-05-10T22:38:08.000000Z",
        "updated_at": "2021-05-15T16:27:24.000000Z",
        "deleted_at": null
    },
    {
        "id": 5,
        "provider_id": 1,
        "description": "Conta de agua",
        "value": 10.1,
        "payed_at": "2021-05-15T16:27:32.000000Z",
        "created_at": "2021-05-10T22:39:02.000000Z",
        "updated_at": "2021-05-15T16:27:32.000000Z",
        "deleted_at": null
    },
    {
        "id": 8,
        "provider_id": 1,
        "description": "Conta de internet",
        "value": 123.21,
        "payed_at": "2021-05-15T16:27:35.000000Z",
        "created_at": "2021-05-10T22:40:45.000000Z",
        "updated_at": "2021-05-15T16:27:35.000000Z",
        "deleted_at": null
    }
]</code></pre>
<div id="execution-results-GETapi-account-debts-report-payed" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts-report-payed"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts-report-payed"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts-report-payed" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts-report-payed"></code></pre>
</div>
<form id="form-GETapi-account-debts-report-payed" data-method="GET" data-path="api/account-debts/report/payed" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts-report-payed', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts-report-payed" onclick="tryItOut('GETapi-account-debts-report-payed');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts-report-payed" onclick="cancelTryOut('GETapi-account-debts-report-payed');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts-report-payed" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts/report/payed</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts-report-payed" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts-report-payed" data-component="header"></label>
</p>
</form>
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                            </div>
            </div>
</div>
<script>
    $(function () {
        var languages = ["bash","javascript"];
        setupLanguages(languages);
    });
</script>
</body>
</html>