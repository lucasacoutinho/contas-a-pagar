<?php

namespace Database\Factories;

use App\Models\Provider;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProviderFactory extends Factory
{
    protected $model = Provider::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'address' => Str::random(20),
            'phone' => $this->faker->phoneNumber(),
            'email' => Str::random(7).'@example.com',
        ];
    }
}
