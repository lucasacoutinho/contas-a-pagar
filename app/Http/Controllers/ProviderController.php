<?php

namespace App\Http\Controllers;

use Domain\Provider\Store as RegrasDeNegocio;
use App\Models\Provider as Model;
use App\Http\Resources\Provider\Provider as Resource;
use App\Http\Requests\Provider\{ProviderStore as RequestStore, ProviderUpdate as RequestUpdate};

class ProviderController extends Controller
{
    /**
     * Listar Fornecedores
     *
     * Retonar todos os registros do banco
     * @group Fornecedor
     * @responseFile Response/Fornecedor/Listar.json
     */
    public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        abort_if(!auth()->user()->tokenCan('read'), 403, 'This action is unauthorized.');
        return Resource::collection(Model::with('debts')->paginate());
    }

    /**
     * Criar fornecedor
     *
     * Cria um novo fornecedor
     * @group Fornecedor
     * @responseFile 201 Response/Fornecedor/Detalhar.json
     * @responseFile 422 Response/Fornecedor/ValidarCriar.json
     */
    public function store(RequestStore $request): Resource
    {
        $provider = (new RegrasDeNegocio)->store($request);
        return new Resource($provider);
    }

    /**
     * Detalhar Fornecedor
     *
     * Retorna os dados do fornecedor
     * @group Fornecedor
     * @urlParam provider integer required O id do fornecedor
     * @responseFile Response/Fornecedor/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Provider] 3"}
     */
    public function show(Model $provider): Resource
    {
        abort_if(!auth()->user()->tokenCan('read'), 403, 'This action is unauthorized.');
        return new Resource($provider);
    }

    /**
     * Atualizar Fornecedor
     *
     * Atualiza os dados do fornecedor
     * @group Fornecedor
     * @urlParam provider integer required O id do fornecedor
     * @responseFile Response/Fornecedor/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Provider] 3"}
     * @responseFile 422 Response/Fornecedor/ValidarAtualizar.json
     */
    public function update(RequestUpdate $request, Model $provider): Resource
    {
        $provider->update($request->all());
        return new Resource($provider);
    }

    /**
     * Excluir Fornecedor
     *
     * Exclui um fornecedor
     * @group Fornecedor
     * @urlParam provider integer required O id do fornecedor
     * @response 404 {"message": "No query results for model [App\\Models\\Provider] 3"}
     */
    public function destroy(Model $provider): void
    {
        abort_if(!auth()->user()->tokenCan('delete'), 403, 'This action is unauthorized.');
        $provider->delete();
    }
}
