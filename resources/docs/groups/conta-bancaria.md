# Conta Bancaria


## Listar Contas Bancarias

<small class="badge badge-darkred">requires authentication</small>

Retonar todos os registros do banco

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/bank-account" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/bank-account"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [
        {
            "account_id": 1,
            "bank_name": "BB",
            "agency_number": "BRA013",
            "account_number": "39401039",
            "balance_start": 200.16,
            "created_at": "2021-05-03T17:50:16.000000Z",
            "updated_at": "2021-05-03T19:45:28.000000Z"
        },
        {
            "account_id": 2,
            "bank_name": "Kunze, Kilback and Weber",
            "agency_number": "28845381",
            "account_number": "2003427881",
            "balance_start": 154491.26,
            "created_at": "2021-05-03T17:50:16.000000Z",
            "updated_at": "2021-05-03T17:50:16.000000Z"
        }
    ],
    "links": {
        "first": "http:\/\/127.0.0.1:8000\/api\/bank-account?page=1",
        "last": "http:\/\/127.0.0.1:8000\/api\/bank-account?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/bank-account?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Próximo &raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/127.0.0.1:8000\/api\/bank-account",
        "per_page": 15,
        "to": 13,
        "total": 13
    }
}
```
<div id="execution-results-GETapi-bank-account" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-bank-account"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-bank-account"></code></pre>
</div>
<div id="execution-error-GETapi-bank-account" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-bank-account"></code></pre>
</div>
<form id="form-GETapi-bank-account" data-method="GET" data-path="api/bank-account" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-bank-account', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-bank-account" onclick="tryItOut('GETapi-bank-account');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-bank-account" onclick="cancelTryOut('GETapi-bank-account');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-bank-account" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/bank-account</code></b>
</p>
<p>
<label id="auth-GETapi-bank-account" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-bank-account" data-component="header"></label>
</p>
</form>


## Criar Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Cria uma nova conta bancaria

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/bank-account" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"bank_name":"ITAU","agency_number":"BRA01","account_number":"2003427881","balance_start":154491.26}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/bank-account"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "bank_name": "ITAU",
    "agency_number": "BRA01",
    "account_number": "2003427881",
    "balance_start": 154491.26
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (201):

```json
{
    "data": {
        "account_id": 1,
        "bank_name": "BB",
        "agency_number": "BRA013",
        "account_number": "39401039",
        "balance_start": 200.16,
        "created_at": "2021-05-03T17:50:16.000000Z",
        "updated_at": "2021-05-03T19:45:28.000000Z"
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "bank_name": [
            "O campo bank_name é obrigatório.",
            "O campo bank_name não pode ser superior a 255 caracteres."
        ],
        "agency_number": [
            "O campo agency_number é obrigatório.",
            "O campo agency_number não pode ser superior a 10 caracteres."
        ],
        "account_number": [
            "O campo account_number é obrigatório.",
            "O campo account_number não pode ser superior a 10 caracteres."
        ],
        "balance_start": [
            "O campo balance_start é obrigatório.",
            "O campo balance_start deve ser um número."
        ]
    }
}
```
<div id="execution-results-POSTapi-bank-account" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-bank-account"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-bank-account"></code></pre>
</div>
<div id="execution-error-POSTapi-bank-account" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-bank-account"></code></pre>
</div>
<form id="form-POSTapi-bank-account" data-method="POST" data-path="api/bank-account" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-bank-account', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-bank-account" onclick="tryItOut('POSTapi-bank-account');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-bank-account" onclick="cancelTryOut('POSTapi-bank-account');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-bank-account" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/bank-account</code></b>
</p>
<p>
<label id="auth-POSTapi-bank-account" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-bank-account" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>bank_name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="bank_name" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Nome do Banco.
</p>
<p>
<b><code>agency_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="agency_number" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Agencia bancaria.
</p>
<p>
<b><code>account_number</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="account_number" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Numero da conta bancaria.
</p>
<p>
<b><code>balance_start</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="balance_start" data-endpoint="POSTapi-bank-account" data-component="body" required  hidden>
<br>
Balanço da conta bancaria.
</p>

</form>


## Detalhar Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Retorna os dados da conta bancaria

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/bank-account/16" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/bank-account/16"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\BankAccount] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "account_id": 1,
        "bank_name": "BB",
        "agency_number": "BRA013",
        "account_number": "39401039",
        "balance_start": 200.16,
        "created_at": "2021-05-03T17:50:16.000000Z",
        "updated_at": "2021-05-03T19:45:28.000000Z"
    }
}
```
<div id="execution-results-GETapi-bank-account--bank_account-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-bank-account--bank_account-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-bank-account--bank_account-"></code></pre>
</div>
<div id="execution-error-GETapi-bank-account--bank_account-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-bank-account--bank_account-"></code></pre>
</div>
<form id="form-GETapi-bank-account--bank_account-" data-method="GET" data-path="api/bank-account/{bank_account}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-bank-account--bank_account-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-bank-account--bank_account-" onclick="tryItOut('GETapi-bank-account--bank_account-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-bank-account--bank_account-" onclick="cancelTryOut('GETapi-bank-account--bank_account-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-bank-account--bank_account-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<label id="auth-GETapi-bank-account--bank_account-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-bank-account--bank_account-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>bank_account</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="bank_account" data-endpoint="GETapi-bank-account--bank_account-" data-component="url" required  hidden>
<br>
O valor do id da conta bancaria
</p>
</form>


## Atualizar Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Atualiza os dados da conta bancaria

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/bank-account/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"bank_name":"ITAU","agency_number":"BRA01","account_number":"2003427881","balance_start":154491.26}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/bank-account/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "bank_name": "ITAU",
    "agency_number": "BRA01",
    "account_number": "2003427881",
    "balance_start": 154491.26
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\BankAccount] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "account_id": 1,
        "bank_name": "BB",
        "agency_number": "BRA013",
        "account_number": "39401039",
        "balance_start": 200.16,
        "created_at": "2021-05-03T17:50:16.000000Z",
        "updated_at": "2021-05-03T19:45:28.000000Z"
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "bank_name": [
            "O campo bank_name não pode ser superior a 255 caracteres.",
            "O campo bank_name deve ter um valor."
        ],
        "agency_number": [
            "O campo agency_number não pode ser superior a 10 caracteres.",
            "O campo agency_number deve ter um valor."
        ],
        "account_number": [
            "O campo account_number não pode ser superior a 10 caracteres.",
            "O campo account_number deve ter um valor."
        ],
        "balance_start": [
            "O campo balance_start deve ser um número.",
            "O campo balance_start deve ter um valor."
        ]
    }
}
```
<div id="execution-results-PUTapi-bank-account--bank_account-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-bank-account--bank_account-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-bank-account--bank_account-"></code></pre>
</div>
<div id="execution-error-PUTapi-bank-account--bank_account-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-bank-account--bank_account-"></code></pre>
</div>
<form id="form-PUTapi-bank-account--bank_account-" data-method="PUT" data-path="api/bank-account/{bank_account}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-bank-account--bank_account-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-bank-account--bank_account-" onclick="tryItOut('PUTapi-bank-account--bank_account-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-bank-account--bank_account-" onclick="cancelTryOut('PUTapi-bank-account--bank_account-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-bank-account--bank_account-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<label id="auth-PUTapi-bank-account--bank_account-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-bank-account--bank_account-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>bank_account</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="bank_account" data-endpoint="PUTapi-bank-account--bank_account-" data-component="url" required  hidden>
<br>
O valor do id da conta bancaria
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>bank_name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="bank_name" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Nome do Banco.
</p>
<p>
<b><code>agency_number</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="agency_number" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Agencia bancaria.
</p>
<p>
<b><code>account_number</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="account_number" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Numero da conta bancaria.
</p>
<p>
<b><code>balance_start</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="balance_start" data-endpoint="PUTapi-bank-account--bank_account-" data-component="body"  hidden>
<br>
Balanço da conta bancaria.
</p>

</form>


## Excluir Conta Bancaria

<small class="badge badge-darkred">requires authentication</small>

Exclui uma conta bancaria

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/api/bank-account/11" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/bank-account/11"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\BankAccount] 3"
}
```
<div id="execution-results-DELETEapi-bank-account--bank_account-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-bank-account--bank_account-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-bank-account--bank_account-"></code></pre>
</div>
<div id="execution-error-DELETEapi-bank-account--bank_account-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-bank-account--bank_account-"></code></pre>
</div>
<form id="form-DELETEapi-bank-account--bank_account-" data-method="DELETE" data-path="api/bank-account/{bank_account}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-bank-account--bank_account-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-bank-account--bank_account-" onclick="tryItOut('DELETEapi-bank-account--bank_account-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-bank-account--bank_account-" onclick="cancelTryOut('DELETEapi-bank-account--bank_account-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-bank-account--bank_account-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/bank-account/{bank_account}</code></b>
</p>
<p>
<label id="auth-DELETEapi-bank-account--bank_account-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-bank-account--bank_account-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>bank_account</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="bank_account" data-endpoint="DELETEapi-bank-account--bank_account-" data-component="url" required  hidden>
<br>
O valor do id da conta bancaria
</p>
</form>



