<?php

namespace App\Http\Resources\DebtPaymentHistory;

use Illuminate\Http\Resources\Json\JsonResource;

class History extends JsonResource
{
    public function toArray($request)
    {
        return [
            'history_id' => $this->getKey(),
            'user_id'    => $this->user_id,
            'user_name'  => $this->user->name,
            'is_payment' => $this->is_payment,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
