<?php

namespace App\Http\Requests\BankAccount;

class BankAccountUpdate extends BankAccountDoc
{
    public function authorize() {
        return auth()->user()->tokenCan('update');
    }

    public function rules()
    {
        return [
            'bank_name'      => ['filled', 'max:255'],
            'agency_number'  => ['filled', 'max:10'],
            'account_number' => ['filled', 'max:10'],
            'balance_start'  => ['filled', 'numeric'],
        ];
    }
}
