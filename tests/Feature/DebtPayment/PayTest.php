<?php

namespace Tests\Feature\DebtPayment;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;

class PayTest extends TestCase
{
    private const ROUTE = 'pay';

    public function test_success()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['update'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $debt->getKey()));

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    'debt_id'     => $debt->getKey(),
                    'provider_id' => $debt->provider_id,
                    'description' => $debt->description,
                    'value'       => $debt->value,
                    'payed_at'    => $response['data']['payed_at'],
                    'created_at'  => $debt->created_at,
                    'updated_at'  => $debt->updated_at,
                    'history'     => $response['data']['history'],
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $debt->getKey()));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $response = $this->putJson(route(self::ROUTE, $debt->getKey()));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
