<?php

namespace Tests\Feature\Debt;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;

class StoreTest extends TestCase
{
    private const ROUTE = 'account-debts.store';

    public function test_success()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['create'])->plainTextToken;

        $response = $this->withToken($token)->postJson(route(self::ROUTE), $debt);

        $response->assertStatus(201)
            ->assertJsonFragment([
                "data" => [
                    'debt_id'     => $response['data']['debt_id'],
                    'provider_id' => $debt['provider_id'],
                    'description' => $debt['description'],
                    'value'       => $debt['value'],
                    'payed_at'    => $response['data']['payed_at'],
                    'created_at'  => $response['data']['created_at'],
                    'updated_at'  => $response['data']['updated_at'],
                    'history'     => [],
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->postJson(route(self::ROUTE), $debt);

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->make()->toArray();

        $response = $this->postJson(route(self::ROUTE), $debt);

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
