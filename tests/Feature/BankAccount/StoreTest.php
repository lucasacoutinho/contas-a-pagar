<?php

namespace Tests\Feature\BankAccount;

use Tests\TestCase;
use App\Models\User;
use App\Models\BankAccount;

class StoreTest extends TestCase
{
    private const ROUTE = 'bank-account.store';

    public function test_success()
    {
        $account = BankAccount::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['create'])->plainTextToken;

        $response = $this->withToken($token)->postJson(route(self::ROUTE), $account);

        $response->assertStatus(201)
            ->assertJsonFragment([
                "data" => [
                    "account_id"     => $response['data']['account_id'],
                    "bank_name"      => $account['bank_name'],
                    "agency_number"  => $account['agency_number'],
                    "account_number" => $account['account_number'],
                    "balance_start"  => $account['balance_start'],
                    "created_at"     => $response['data']['created_at'],
                    "updated_at"     => $response['data']['updated_at'],
                ]
            ]);

        $this->assertDatabaseHas('bank_accounts', $account);
    }

    public function test_failure_prohibited()
    {
        $account = BankAccount::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->postJson(route(self::ROUTE), $account);

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $account = BankAccount::factory()->make()->toArray();

        $response = $this->postJson(route(self::ROUTE), $account);

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
