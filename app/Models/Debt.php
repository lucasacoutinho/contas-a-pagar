<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Debt extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'debts';

    protected $fillable = [
        'provider_id',
        'description',
        'value',
    ];

    protected $casts = [
        'provider_id' => 'integer',
        'value' => 'float',
        'payed_at' => 'datetime',
    ];

    protected $hidden = [
        'deleted_at',
    ];

    protected $dates = [
        'payed_at'
    ];

    public function history() {
        return $this->hasMany(DebtPaymentHistory::class, 'debt_id', 'id');
    }
}
