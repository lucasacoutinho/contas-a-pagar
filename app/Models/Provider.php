<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Provider extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'providers';

    protected $fillable = [
        "name",
        "phone",
        "address",
        "email",
    ];

    public function debts() {
        return $this->hasMany(Debt::class, 'provider_id', 'id');
    }
}
