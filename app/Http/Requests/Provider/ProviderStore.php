<?php

namespace App\Http\Requests\Provider;

class ProviderStore extends ProviderDoc
{
    public function authorize()
    {
        return auth()->user()->tokenCan('create');
    }

    public function rules()
    {
        return [
            "name"    => ['required', 'max:255'],
            "address" => ['required', 'max:255'],
            "phone"   => ['nullable', 'max:20'],
            "email"   => ['nullable', 'max:20', 'email'],
            "debts"   => ['filled', 'array'],
            "debts.*.description" => ['required', 'string', 'max:255'],
            "debts.*.value"       => ['required', 'numeric'],
        ];
    }
}
