<?php

namespace Domain\DebtPayment;

use App\Models\Debt as DebtModel;
use Illuminate\Support\Facades\DB;

class Payment
{
    public function pay(DebtModel $accountDebt): void
    {
        DB::transaction(function () use ($accountDebt) {
            $accountDebt->payed_at = now();
            $accountDebt->save();

            $accountDebt->history()->create([
                'user_id' => auth()->id()
            ]);
        });
    }
}
