<?php

namespace App\Http\Controllers;

use App\Models\Debt;
use Domain\DebtPayment\Payment;
use App\Http\Resources\Debt\Debt as Resource;

class DebtPaymentController extends Controller
{
    /**
     * Liquidar conta a pagar
     *
     * Realiza a liquidação de uma conta a pagar
     * @group Conta a Pagar Liquidar/Estornar
     * @urlParam account_debt integer required O id da conta a pagar
     * @responseFile Response/ContasAPagar/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Debt] 3"}
     */
    public function __invoke(Debt $accountDebt)
    {
        abort_if(!auth()->user()->tokenCan('update'), 403, 'This action is unauthorized.');
        (new Payment)->pay($accountDebt);
        return new Resource($accountDebt);
    }
}
