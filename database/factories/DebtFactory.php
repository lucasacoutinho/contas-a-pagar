<?php

namespace Database\Factories;

use App\Models\Debt;
use App\Models\Provider;
use Illuminate\Database\Eloquent\Factories\Factory;

class DebtFactory extends Factory
{
    protected $model = Debt::class;

    public function definition()
    {
        return [
            'provider_id' => Provider::inRandomOrder()->first()?->getKey() ?? null,
            'description' => $this->faker->words(asText: true),
            'value' => $this->faker->randomFloat(2, 10, 200),
        ];
    }

    public function payed()
    {
        return $this->state(function (array $attributes) {
            return [
                'payed_at' => $this->faker->dateTime()
            ];
        });
    }
}
