## 1. Requerimentos
- Nodejs
- PHP 8
- **[swoole](https://www.swoole.co.uk/docs/get-started/installation)**
- Linux/WSL/MACOS

## 2. Instalação

```
composer i
```
```
npm i
```
```
php artisan migrate
```
```
php artisan octane:start --watch
```

## 3. Documentação

- **[SWAGGER](https://app.swaggerhub.com/apis-docs/lethargiclesson/Laravel-API/1.0.0#/)**
- **[POSTMAN](https://documenter.getpostman.com/view/13570758/TzRX7QT9#f9777144-cdd6-479b-9647-342ac82f39b4)** 


## 4. Uso
1. Realiza o processo de [cadastro](http://localhost:8000/register)
2. [Gerar token](http://localhost:8000/user/api-tokens) para utilizar a API

## 5. Informativos

- **[Laradock](https://laradock.io​)**
- **[Preparar requisição para a validação](https://laravel.com/docs/8.x/)**
- **[Repositório da aplicação](https://gitlab.com/moreiraandre/api-contas-a-pagar)**
- **[Commits Semânticos](https://ildaneta.dev/posts/entendo-a-import%C3%A2ncia-dos-commits-sem%C3%A2nticos/)**
- **[Customizar Stubs Artisan](https://laravel.com/docs/8.x/artisan#stub-customization)**
- **[Scribe](https://scribe.readthedocs.io/en/latest/guide-getting-started.html)**
- **[Pacote Traduções Laravel](https://github.com/lucascudo/laravel-pt-BR-localization)**
- **[Octane](https://github.com/laravel/octane)**
- **[Laravel IDE Helper](https://github.com/barryvdh/laravel-ide-helper)**
- **[Swoole  IDE Helper](https://github.com/swoole/ide-helper)**
- **[Laravel DomPDF](https://github.com/barryvdh/laravel-dompdf)**
- **[Laravel testes](https://laravel.com/docs/8.x/http-tests)**
