# Relatorio


## Contas pendentes

<small class="badge badge-darkred">requires authentication</small>

Retorna a listagem de contas pendentes de pagamento. Utilize header Accept application/json

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts/report/pending" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/report/pending"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 3,
        "provider_id": 1,
        "description": "Conta de luz",
        "value": 10.1,
        "payed_at": null,
        "created_at": "2021-05-10T22:38:08.000000Z",
        "updated_at": "2021-05-15T16:27:24.000000Z",
        "deleted_at": null
    },
    {
        "id": 5,
        "provider_id": 1,
        "description": "Conta de agua",
        "value": 10.1,
        "payed_at": null,
        "created_at": "2021-05-10T22:39:02.000000Z",
        "updated_at": "2021-05-15T16:27:32.000000Z",
        "deleted_at": null
    },
    {
        "id": 8,
        "provider_id": 1,
        "description": "Conta de internet",
        "value": 123.21,
        "payed_at": null,
        "created_at": "2021-05-10T22:40:45.000000Z",
        "updated_at": "2021-05-15T16:27:35.000000Z",
        "deleted_at": null
    }
]
```
<div id="execution-results-GETapi-account-debts-report-pending" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts-report-pending"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts-report-pending"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts-report-pending" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts-report-pending"></code></pre>
</div>
<form id="form-GETapi-account-debts-report-pending" data-method="GET" data-path="api/account-debts/report/pending" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts-report-pending', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts-report-pending" onclick="tryItOut('GETapi-account-debts-report-pending');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts-report-pending" onclick="cancelTryOut('GETapi-account-debts-report-pending');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts-report-pending" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts/report/pending</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts-report-pending" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts-report-pending" data-component="header"></label>
</p>
</form>


## Contas pagas

<small class="badge badge-darkred">requires authentication</small>

Retorna a listagem de contas pagas. Utilize header Accept application/json

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts/report/payed" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/report/payed"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 3,
        "provider_id": 1,
        "description": "Conta de luz",
        "value": 10.1,
        "payed_at": "2021-05-15T16:27:24.000000Z",
        "created_at": "2021-05-10T22:38:08.000000Z",
        "updated_at": "2021-05-15T16:27:24.000000Z",
        "deleted_at": null
    },
    {
        "id": 5,
        "provider_id": 1,
        "description": "Conta de agua",
        "value": 10.1,
        "payed_at": "2021-05-15T16:27:32.000000Z",
        "created_at": "2021-05-10T22:39:02.000000Z",
        "updated_at": "2021-05-15T16:27:32.000000Z",
        "deleted_at": null
    },
    {
        "id": 8,
        "provider_id": 1,
        "description": "Conta de internet",
        "value": 123.21,
        "payed_at": "2021-05-15T16:27:35.000000Z",
        "created_at": "2021-05-10T22:40:45.000000Z",
        "updated_at": "2021-05-15T16:27:35.000000Z",
        "deleted_at": null
    }
]
```
<div id="execution-results-GETapi-account-debts-report-payed" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts-report-payed"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts-report-payed"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts-report-payed" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts-report-payed"></code></pre>
</div>
<form id="form-GETapi-account-debts-report-payed" data-method="GET" data-path="api/account-debts/report/payed" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts-report-payed', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts-report-payed" onclick="tryItOut('GETapi-account-debts-report-payed');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts-report-payed" onclick="cancelTryOut('GETapi-account-debts-report-payed');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts-report-payed" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts/report/payed</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts-report-payed" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts-report-payed" data-component="header"></label>
</p>
</form>



