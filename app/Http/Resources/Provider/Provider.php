<?php

namespace App\Http\Resources\Provider;

use App\Http\Resources\Debt\Debt;
use Illuminate\Http\Resources\Json\JsonResource;

class Provider extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id"         => $this->id,
            "name"       => $this->name,
            "phone"      => $this->phone,
            "address"    => $this->address,
            "email"      => $this->email,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "debts"      => Debt::collection($this->debts),
        ];
    }
}
