<?php

namespace Tests\Feature\Provider;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    private const ROUTE = 'provider.store';

    public function test_success()
    {
        $provider = Provider::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['create'])->plainTextToken;

        $response = $this->withToken($token)->postJson(route(self::ROUTE), $provider);

        $response->assertStatus(201)
            ->assertJsonFragment([
                "data" => [
                    "id"         => $response['data']['id'],
                    "name"       => $provider['name'],
                    "phone"      => $provider['phone'],
                    "address"    => $provider['address'],
                    "email"      => $provider['email'],
                    "created_at" => $response['data']['created_at'],
                    "updated_at" => $response['data']['updated_at'],
                    "debts"      => [],
                ]
            ]);
    }

    public function test_success_with_debts()
    {
        $provider = Provider::factory()->make()->toArray();
        $debts = Debt::factory(['provider_id' => null])->count(3)->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['create'])->plainTextToken;

        $provider = array_merge($provider, ['debts' => $debts]);

        $response = $this->withToken($token)->postJson(route(self::ROUTE), $provider);

        $debtsCollection = [];
        foreach ($response['data']['debts'] as $key => $debt) {
            $debtsCollection[] = [
                'debt_id'     => $debt['debt_id'],
                'provider_id' => $response['data']['id'],
                'description' => $debts[$key]['description'],
                'value'       => $debts[$key]['value'],
                'payed_at'    => $debt['payed_at'],
                'created_at'  => $debt['created_at'],
                'updated_at'  => $debt['updated_at'],
                'history'     => [],
            ];
        }

        $response->assertStatus(201)
            ->assertJsonFragment([
                "data" => [
                    "id"         => $response['data']['id'],
                    "name"       => $provider['name'],
                    "phone"      => $provider['phone'],
                    "address"    => $provider['address'],
                    "email"      => $provider['email'],
                    "created_at" => $response['data']['created_at'],
                    "updated_at" => $response['data']['updated_at'],
                    "debts"      => $debtsCollection,
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->postJson(route(self::ROUTE));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $response = $this->postJson(route(self::ROUTE));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
