<?php

namespace Domain\PDF;

interface PdfInterface {
    public function gerar(string $view, array $dados, string|null $arquivo);
}
