<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>RELATORIO CONTAS PAGAS</h1>
    <table>
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Valor</th>
                <th>Criada em</th>
                <th>Paga em</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($contasPagas as $conta)
            <tr>
                <td>{{$conta->description}}</td>
                <td>{{$conta->value}}</td>
                <td>{{$conta->created_at->format('d/m/Y H:i:s')}}</td>
                <td>{{$conta->payed_at->format('d/m/Y H:i:s')}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>
