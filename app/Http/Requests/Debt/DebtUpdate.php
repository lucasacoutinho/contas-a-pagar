<?php

namespace App\Http\Requests\Debt;

class DebtUpdate extends DebtDoc
{
    public function authorize()
    {
        return auth()->user()->tokenCan('update');
    }

    public function rules()
    {
        return [
            'provider_id' => ['filled', 'integer', 'exists:providers,id'],
            'description' => ['filled', 'string', 'max:255'],
            'value'       => ['filled', 'numeric'],
        ];
    }
}
