<?php

namespace App\Http\Resources\Debt;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DebtPaymentHistory\History;

class Debt extends JsonResource
{
    public function toArray($request)
    {
        return [
            'debt_id'     => $this->getKey(),
            'provider_id' => $this->provider_id,
            'description' => $this->description,
            'value'       => $this->value,
            'payed_at'    => $this->payed_at,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'history'     => History::collection($this->history)
        ];
    }
}
