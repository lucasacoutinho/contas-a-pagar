# Contas Pedentes


## Listar todos as contas pendentes

<small class="badge badge-darkred">requires authentication</small>

Retonar todas contas pendentes

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [
        {
            "debt_id": 1,
            "provider_id": 1,
            "description": "teste teste teste",
            "value": 10.1,
            "payed_at": null,
            "created_at": "2021-05-08T14:43:35.000000Z",
            "updated_at": "2021-05-13T17:48:45.000000Z",
            "history": [
                {
                    "history_id": 1,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": true,
                    "created_at": "2021-05-13T16:48:51.000000Z",
                    "updated_at": "2021-05-13T16:48:51.000000Z"
                },
                {
                    "history_id": 2,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": true,
                    "created_at": "2021-05-13T16:50:52.000000Z",
                    "updated_at": "2021-05-13T16:50:52.000000Z"
                },
                {
                    "history_id": 3,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": false,
                    "created_at": "2021-05-13T16:58:41.000000Z",
                    "updated_at": "2021-05-13T16:58:41.000000Z"
                }
            ]
        },
        {
            "debt_id": 3,
            "provider_id": 1,
            "description": "teste teste teste",
            "value": 10.1,
            "payed_at": null,
            "created_at": "2021-05-10T22:38:08.000000Z",
            "updated_at": "2021-05-10T22:38:08.000000Z",
            "history": []
        },
        {
            "debt_id": 11,
            "provider_id": 72,
            "description": "teste teste teste",
            "value": 4012.12,
            "payed_at": null,
            "created_at": "2021-05-13T13:06:44.000000Z",
            "updated_at": "2021-05-13T17:53:32.000000Z",
            "history": [
                {
                    "history_id": 6,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": true,
                    "created_at": "2021-05-13T17:21:11.000000Z",
                    "updated_at": "2021-05-13T17:21:11.000000Z"
                },
                {
                    "history_id": 12,
                    "user_id": 1,
                    "user_name": "Rosalyn Winters",
                    "is_payment": false,
                    "created_at": "2021-05-13T17:53:32.000000Z",
                    "updated_at": "2021-05-13T17:53:32.000000Z"
                }
            ]
        }
    ],
    "links": {
        "first": "http:\/\/127.0.0.1:8000\/api\/account-debts?page=1",
        "last": "http:\/\/127.0.0.1:8000\/api\/account-debts?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "links": [
            {
                "url": null,
                "label": "&laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/account-debts?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Próximo &raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/127.0.0.1:8000\/api\/account-debts",
        "per_page": 15,
        "to": 10,
        "total": 10
    }
}
```
<div id="execution-results-GETapi-account-debts" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts"></code></pre>
</div>
<form id="form-GETapi-account-debts" data-method="GET" data-path="api/account-debts" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts" onclick="tryItOut('GETapi-account-debts');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts" onclick="cancelTryOut('GETapi-account-debts');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts" data-component="header"></label>
</p>
</form>


## Criar conta pendente

<small class="badge badge-darkred">requires authentication</small>

Cria uma nova conta pendente

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/account-debts" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"provider_id":1234,"description":"CONTA DE LUZ","value":10101.12}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "provider_id": 1234,
    "description": "CONTA DE LUZ",
    "value": 10101.12
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (201):

```json
{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "provider_id": [
            "O campo provider_id é obrigatório.",
            "O campo provider_id deve ser um número inteiro.",
            "O campo provider_id selecionado é inválido."
        ],
        "description": [
            "O campo description é obrigatório.",
            "O campo description não pode ser superior a 255 caracteres."
        ],
        "value": [
            "O campo value é obrigatório.",
            "O campo value deve ser um número."
        ]
    }
}
```
<div id="execution-results-POSTapi-account-debts" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-account-debts"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-account-debts"></code></pre>
</div>
<div id="execution-error-POSTapi-account-debts" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-account-debts"></code></pre>
</div>
<form id="form-POSTapi-account-debts" data-method="POST" data-path="api/account-debts" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-account-debts', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-account-debts" onclick="tryItOut('POSTapi-account-debts');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-account-debts" onclick="cancelTryOut('POSTapi-account-debts');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-account-debts" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/account-debts</code></b>
</p>
<p>
<label id="auth-POSTapi-account-debts" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-account-debts" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>provider_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider_id" data-endpoint="POSTapi-account-debts" data-component="body" required  hidden>
<br>
id do fornecedor ao qual se tem a divida.
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="description" data-endpoint="POSTapi-account-debts" data-component="body" required  hidden>
<br>
Descrição da divida.
</p>
<p>
<b><code>value</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="value" data-endpoint="POSTapi-account-debts" data-component="body" required  hidden>
<br>
Valor da divida.
</p>

</form>


## Detalhar conta pendente

<small class="badge badge-darkred">requires authentication</small>

Retorna os dados da conta pendente

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/account-debts/17" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/17"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Debt] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-GETapi-account-debts--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-account-debts--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-account-debts--account_debt-"></code></pre>
</div>
<div id="execution-error-GETapi-account-debts--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-account-debts--account_debt-"></code></pre>
</div>
<form id="form-GETapi-account-debts--account_debt-" data-method="GET" data-path="api/account-debts/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-account-debts--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-account-debts--account_debt-" onclick="tryItOut('GETapi-account-debts--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-account-debts--account_debt-" onclick="cancelTryOut('GETapi-account-debts--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-account-debts--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<label id="auth-GETapi-account-debts--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-account-debts--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="GETapi-account-debts--account_debt-" data-component="url" required  hidden>
<br>
O id do debito
</p>
</form>


## Atualizar conta pendente

<small class="badge badge-darkred">requires authentication</small>

Atualiza os dados da conta pendente

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/account-debts/18" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"provider_id":1234,"description":"CONTA DE LUZ","value":10101.12}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/18"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "provider_id": 1234,
    "description": "CONTA DE LUZ",
    "value": 10101.12
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Debt] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "provider_id": [
            "O campo provider_id deve ter um valor.",
            "O campo provider_id deve ser um número inteiro.",
            "O campo provider_id selecionado é inválido."
        ],
        "description": [
            "O campo description deve ter um valor.",
            "O campo description não pode ser superior a 255 caracteres."
        ],
        "value": [
            "O campo value deve ter um valor.",
            "O campo value deve ser um número."
        ]
    }
}
```
<div id="execution-results-PUTapi-account-debts--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-account-debts--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-account-debts--account_debt-"></code></pre>
</div>
<div id="execution-error-PUTapi-account-debts--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-account-debts--account_debt-"></code></pre>
</div>
<form id="form-PUTapi-account-debts--account_debt-" data-method="PUT" data-path="api/account-debts/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-account-debts--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-account-debts--account_debt-" onclick="tryItOut('PUTapi-account-debts--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-account-debts--account_debt-" onclick="cancelTryOut('PUTapi-account-debts--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-account-debts--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<label id="auth-PUTapi-account-debts--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-account-debts--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="PUTapi-account-debts--account_debt-" data-component="url" required  hidden>
<br>
O id do debito
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>provider_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="provider_id" data-endpoint="PUTapi-account-debts--account_debt-" data-component="body"  hidden>
<br>
id do fornecedor ao qual se tem a divida.
</p>
<p>
<b><code>description</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="description" data-endpoint="PUTapi-account-debts--account_debt-" data-component="body"  hidden>
<br>
Descrição da divida.
</p>
<p>
<b><code>value</code></b>&nbsp;&nbsp;<small>number</small>     <i>optional</i> &nbsp;
<input type="number" name="value" data-endpoint="PUTapi-account-debts--account_debt-" data-component="body"  hidden>
<br>
Valor da divida.
</p>

</form>


## Excluir conta pendente

<small class="badge badge-darkred">requires authentication</small>

Exclui uma conta pendente

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/api/account-debts/8" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/8"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Debt] 3"
}
```
<div id="execution-results-DELETEapi-account-debts--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-account-debts--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-account-debts--account_debt-"></code></pre>
</div>
<div id="execution-error-DELETEapi-account-debts--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-account-debts--account_debt-"></code></pre>
</div>
<form id="form-DELETEapi-account-debts--account_debt-" data-method="DELETE" data-path="api/account-debts/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-account-debts--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-account-debts--account_debt-" onclick="tryItOut('DELETEapi-account-debts--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-account-debts--account_debt-" onclick="cancelTryOut('DELETEapi-account-debts--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-account-debts--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/account-debts/{account_debt}</code></b>
</p>
<p>
<label id="auth-DELETEapi-account-debts--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-account-debts--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="DELETEapi-account-debts--account_debt-" data-component="url" required  hidden>
<br>
O id do debito
</p>
</form>



