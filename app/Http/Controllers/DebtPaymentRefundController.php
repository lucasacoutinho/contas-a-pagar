<?php

namespace App\Http\Controllers;

use App\Models\Debt;
use Domain\DebtPayment\Refund;
use App\Http\Resources\Debt\Debt as Resource;

class DebtPaymentRefundController extends Controller
{
    /**
     * Estornar valor da transação
     *
     * Realiza o estorno de um valor pago
     * @group Conta a Pagar Liquidar/Estornar
     * @urlParam account_debt integer required O id da conta a pagar
     * @responseFile Response/ContasAPagar/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Debt] 3"}
     */
    public function __invoke(Debt $accountDebt)
    {
        abort_if(!auth()->user()->tokenCan('update'), 403, 'This action is unauthorized.');
        (new Refund)->refund($accountDebt);
        return new Resource($accountDebt);
    }
}
