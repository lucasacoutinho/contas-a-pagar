<?php

namespace App\Http\Controllers;

use App\Models\BankAccount as Model;
use App\Http\Resources\BankAccount\BankAccount as Resource;
use App\Http\Requests\BankAccount\{BankAccountStore as RequestStore, BankAccountUpdate as RequestUpdate};

class BankAccountController extends Controller
{
    /**
     * Listar Contas Bancarias
     *
     * Retonar todos os registros do banco
     * @group Conta Bancaria
     * @responseFile Response/ContaBancaria/Listar.json
     */
    public function index()
    {
        abort_if(!auth()->user()->tokenCan('read'), 403, 'This action is unauthorized.');

        return Resource::collection(Model::paginate());
    }

    /**
     * Criar Conta Bancaria
     *
     * Cria uma nova conta bancaria
     * @group Conta Bancaria
     * @responseFile 201 Response/ContaBancaria/Detalhar.json
     * @responseFile 422 Response/ContaBancaria/ValidarCriar.json
     */
    public function store(RequestStore $request)
    {
        return new Resource(Model::create($request->all()));
    }

    /**
     * Detalhar Conta Bancaria
     *
     * Retorna os dados da conta bancaria
     * @group Conta Bancaria
     * @urlParam bank_account integer required O valor do id da conta bancaria
     * @responseFile Response/ContaBancaria/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\BankAccount] 3"}
     */
    public function show(Model $bankAccount)
    {
        abort_if(!auth()->user()->tokenCan('read'), 403, 'This action is unauthorized.');
        return new Resource($bankAccount);
    }

    /**
     * Atualizar Conta Bancaria
     *
     * Atualiza os dados da conta bancaria
     * @group Conta Bancaria
     * @urlParam bank_account integer required O valor do id da conta bancaria
     * @responseFile Response/ContaBancaria/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\BankAccount] 3"}
     * @responseFile 422 Response/ContaBancaria/ValidarAtualizar.json
     */
    public function update(RequestUpdate $request, Model $bankAccount)
    {
        $bankAccount->update($request->all());
        return new Resource($bankAccount);
    }

    /**
     * Excluir Conta Bancaria
     *
     * Exclui uma conta bancaria
     * @group Conta Bancaria
     * @urlParam bank_account integer required O valor do id da conta bancaria
     * @response 404 {"message": "No query results for model [App\\Models\\BankAccount] 3"}
     */
    public function destroy(Model $bankAccount)
    {
        abort_if(!auth()->user()->tokenCan('delete'), 403, 'This action is unauthorized.');
        $bankAccount->delete();
    }
}
