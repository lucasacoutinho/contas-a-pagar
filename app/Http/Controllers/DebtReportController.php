<?php

namespace App\Http\Controllers;

use Domain\Report\DebtReport;

class DebtReportController extends Controller
{
    /**
     * Contas pendentes
     *
     * Retorna a listagem de contas pendentes de pagamento. Utilize header Accept application/json
     * @group Relatorio
     * @header Accept application/json
     * @responseFile 200 Response/Relatorio/Pendentes.json
     */
    public function pending(DebtReport $report)
    {
        abort_if(!auth()->user()->tokenCan('report'), 403, 'This action is unauthorized.');
        return $report->peding();
    }

    /**
     * Contas pagas
     *
     * Retorna a listagem de contas pagas. Utilize header Accept application/json
     * @group Relatorio
     * @header Accept application/json
     * @responseFile 200 Response/Relatorio/Liquidadas.json
     */
    public function payed(DebtReport $report)
    {
        abort_if(!auth()->user()->tokenCan('report'), 403, 'This action is unauthorized.');
        return $report->payed();
    }
}
