<?php

namespace App\Http\Resources\BankAccount;

use Illuminate\Http\Resources\Json\JsonResource;

class BankAccount extends JsonResource
{
    public function toArray($request)
    {
        return [
            'account_id' => $this->id,
            'bank_name' => $this->bank_name,
            'agency_number' => $this->agency_number,
            'account_number' => $this->account_number,
            'balance_start' => $this->balance_start,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
