<?php

namespace Domain\PDF;

use PDF as Dependencia;

class Pdf implements PdfInterface
{
    public function gerar(string $view, array $dados, string|null $arquivo)
    {
        $pdf = Dependencia::loadView($view, $dados);
        return $pdf->download($arquivo ? "$arquivo.pdf" : "arquivo.pdf")->headers->remove('Content-Length');
    }
}
