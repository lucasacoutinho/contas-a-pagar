<?php

namespace Tests\Feature\Debt;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;

class DestroyTest extends TestCase
{
    private const ROUTE = 'account-debts.destroy';

    public function test_success()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['delete'])->plainTextToken;

        $response = $this->withToken($token)->deleteJson(route(self::ROUTE, $debt->getKey()));

        $response->assertStatus(200)->assertSee('');
    }

    public function test_failure_prohibited()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->deleteJson(route(self::ROUTE, $debt->getKey()));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();

        $response = $this->deleteJson(route(self::ROUTE, $debt->getKey()));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
