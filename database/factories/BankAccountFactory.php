<?php

namespace Database\Factories;

use App\Models\BankAccount;
use Illuminate\Database\Eloquent\Factories\Factory;

class BankAccountFactory extends Factory
{
    protected $model = BankAccount::class;

    public function definition()
    {
        return [
            'bank_name' => $this->faker->company(0),
            'agency_number' => $this->faker->randomNumber(5),
            'account_number' => $this->faker->randomNumber(5),
            'balance_start' => $this->faker->randomFloat(2, max:999999),
        ];
    }
}
