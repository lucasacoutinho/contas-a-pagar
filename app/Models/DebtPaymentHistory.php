<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebtPaymentHistory extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'debt_payment_history';

    protected $fillable = [
        'user_id',
        'is_payment'
    ];

    protected $casts = [
        'user_id'    => 'integer',
        'is_payment' => 'boolean'
    ];


    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
