<?php

namespace Domain\Report;

use Domain\PDF\PdfInterface;
use App\Models\Debt as Model;

class DebtReport
{
    public function __construct(private PdfInterface $pdf)
    {
    }

    public function peding()
    {
        $contasPendetes = Model::whereNull('payed_at')->get();
        if (request()->header('Accept') === 'application/pdf') {
            return $this->pdf->gerar('relatorio.pdf.contas-pendentes', compact('contasPendetes'), 'contas-pendentes');
        }
        return $contasPendetes;
    }

    public function payed()
    {
        $contasPagas = Model::whereNotNull('payed_at')->get();
        if (request()->header('Accept') === 'application/pdf') {
            return $this->pdf->gerar('relatorio.pdf.contas-pagas', compact('contasPagas'), 'contas-pagas');
        }
        return $contasPagas;
    }
}
