<?php

namespace App\Http\Controllers;

use App\Http\Requests\Debt\{DebtStore as RequestStore, DebtUpdate as RequestUpdate};
use App\Http\Resources\Debt\Debt as Resource;
use App\Models\Debt as Model;

class DebtController extends Controller
{
    /**
     * Listar todos as contas pendentes
     *
     * Retonar todas contas pendentes
     * @group Contas Pedentes
     * @responseFile Response/ContasAPagar/Listar.json
     */
    public function index()
    {
        abort_if(!auth()->user()->tokenCan('read'), 403, 'This action is unauthorized.');
        return Resource::collection(Model::with('history.user')->paginate());
    }

    /**
     * Criar conta pendente
     *
     * Cria uma nova conta pendente
     * @group Contas Pedentes
     * @responseFile 201 Response/ContasAPagar/Detalhar.json
     * @responseFile 422 Response/ContasAPagar/ValidarCriar.json
     */
    public function store(RequestStore $request)
    {
        return new Resource(Model::create($request->all()));
    }

    /**
     * Detalhar conta pendente
     *
     * Retorna os dados da conta pendente
     * @group Contas Pedentes
     * @urlParam account_debt integer required O id do debito
     * @responseFile Response/ContasAPagar/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Debt] 3"}
     */
    public function show(Model $accountDebt)
    {
        abort_if(!auth()->user()->tokenCan('read'), 403, 'This action is unauthorized.');
        return new Resource($accountDebt);
    }

    /**
     * Atualizar conta pendente
     *
     * Atualiza os dados da conta pendente
     * @group Contas Pedentes
     * @urlParam account_debt integer required O id do debito
     * @responseFile Response/ContasAPagar/Detalhar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Debt] 3"}
     * @responseFile 422 Response/ContasAPagar/ValidarAtualizar.json
     */
    public function update(RequestUpdate $request, Model $accountDebt)
    {
        $accountDebt->update($request->all());
        return new Resource($accountDebt);
    }

    /**
     * Excluir conta pendente
     *
     * Exclui uma conta pendente
     * @group Contas Pedentes
     * @urlParam account_debt integer required O id do debito
     * @response 404 {"message": "No query results for model [App\\Models\\Debt] 3"}
     */
    public function destroy(Model $accountDebt)
    {
        abort_if(!auth()->user()->tokenCan('delete'), 403, 'This action is unauthorized.');
        $accountDebt->delete();
    }
}
