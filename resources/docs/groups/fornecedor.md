# Fornecedor


## Listar Fornecedores

<small class="badge badge-darkred">requires authentication</small>

Retonar todos os registros do banco

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/provider" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/provider"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "name": "Nome Atualizado 2",
            "phone": "(469) 672-3724",
            "address": "Novo Endereço",
            "email": "harry19@example.com",
            "created_at": "2021-05-01T19:24:55.000000Z",
            "updated_at": "2021-05-01T20:28:19.000000Z",
            "debts": [
                {
                    "debt_id": 1,
                    "provider_id": 1,
                    "description": "teste teste teste",
                    "value": 10.1,
                    "payed_at": null,
                    "created_at": "2021-05-08T14:43:35.000000Z",
                    "updated_at": "2021-05-09T15:03:36.000000Z"
                },
                {
                    "debt_id": 3,
                    "provider_id": 1,
                    "description": "teste teste teste",
                    "value": 10.1,
                    "payed_at": null,
                    "created_at": "2021-05-10T22:38:08.000000Z",
                    "updated_at": "2021-05-10T22:38:08.000000Z"
                }
            ]
        },
        {
            "id": 2,
            "name": "Mr. Leland Turcotte MD",
            "phone": "+18656291273",
            "address": "1225 Dewitt Stravenue Apt. 158\nEast Dexterview, NV 61312",
            "email": "vivian.bartoletti@example.org",
            "created_at": "2021-05-01T19:24:55.000000Z",
            "updated_at": "2021-05-01T19:24:55.000000Z",
            "debts": []
        },
        {
            "id": 5,
            "name": "Rose Graham",
            "phone": "+1 (743) 965-6878",
            "address": "22762 Kristy Pass Apt. 661\nPort Dean, KS 84062-8616",
            "email": "tschumm@example.net",
            "created_at": "2021-05-01T19:24:56.000000Z",
            "updated_at": "2021-05-01T19:24:56.000000Z",
            "debts": []
        }
    ],
    "links": {
        "first": "http:\/\/127.0.0.1:8000\/api\/provider?page=1",
        "last": "http:\/\/127.0.0.1:8000\/api\/provider?page=5",
        "prev": null,
        "next": "http:\/\/127.0.0.1:8000\/api\/provider?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 5,
        "links": [
            {
                "url": null,
                "label": "&laquo; Anterior",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=3",
                "label": "3",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=4",
                "label": "4",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=5",
                "label": "5",
                "active": false
            },
            {
                "url": "http:\/\/127.0.0.1:8000\/api\/provider?page=2",
                "label": "Próximo &raquo;",
                "active": false
            }
        ],
        "path": "http:\/\/127.0.0.1:8000\/api\/provider",
        "per_page": 15,
        "to": 15,
        "total": 63
    }
}
```
<div id="execution-results-GETapi-provider" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-provider"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-provider"></code></pre>
</div>
<div id="execution-error-GETapi-provider" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-provider"></code></pre>
</div>
<form id="form-GETapi-provider" data-method="GET" data-path="api/provider" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-provider', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-provider" onclick="tryItOut('GETapi-provider');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-provider" onclick="cancelTryOut('GETapi-provider');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-provider" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/provider</code></b>
</p>
<p>
<label id="auth-GETapi-provider" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-provider" data-component="header"></label>
</p>
</form>


## Criar fornecedor

<small class="badge badge-darkred">requires authentication</small>

Cria um novo fornecedor

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8000/api/provider" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"FORNECEDOR DE ENERGIA","address":"Avenida Brasil, 200, S\u00e3o Paulo","phone":"(38) 4002-8922","email":"energiaecia@fornecedor.com","debts":[{"description":"CONTA DE ENERGIA","value":99.67}]}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/provider"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "FORNECEDOR DE ENERGIA",
    "address": "Avenida Brasil, 200, S\u00e3o Paulo",
    "phone": "(38) 4002-8922",
    "email": "energiaecia@fornecedor.com",
    "debts": [
        {
            "description": "CONTA DE ENERGIA",
            "value": 99.67
        }
    ]
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (201):

```json
{
    "data": {
        "id": 1,
        "name": "Nome Atualizado 2",
        "phone": "(469) 672-3724",
        "address": "Novo Endereço",
        "email": "harry19@example.com",
        "created_at": "2021-05-01T19:24:55.000000Z",
        "updated_at": "2021-05-01T20:28:19.000000Z",
        "debts": [
            {
                "debt_id": 1,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-08T14:43:35.000000Z",
                "updated_at": "2021-05-09T15:03:36.000000Z"
            },
            {
                "debt_id": 3,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-10T22:38:08.000000Z",
                "updated_at": "2021-05-10T22:38:08.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "O campo name é obrigatório.",
            "O campo name não pode ser superior a 255 caracteres."
        ],
        "address": [
            "O campo address é obrigatório.",
            "O campo address não pode ser superior a 255 caracteres."
        ],
        "email": [
            "O campo email deve ser um endereço de e-mail válido.",
            "O campo email não pode ser superior a 20 caracteres."
        ],
        "phone": [
            "O campo phone não pode ser superior a 20 caracteres."
        ],
        "debts": [
            "O campo debts deve ter um valor.",
            "O campo debts deve ser uma matriz."
        ],
        "debts.0.description": [
            "O campo debts.0.description é obrigatório.",
            "O campo debts.0.description não pode ser superior a 255 caracteres."
        ],
        "debts.0.value": [
            "O campo debts.0.value é obrigatório.",
            "O campo debts.0.value deve ser um número."
        ]
    }
}
```
<div id="execution-results-POSTapi-provider" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-provider"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-provider"></code></pre>
</div>
<div id="execution-error-POSTapi-provider" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-provider"></code></pre>
</div>
<form id="form-POSTapi-provider" data-method="POST" data-path="api/provider" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-provider', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-provider" onclick="tryItOut('POSTapi-provider');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-provider" onclick="cancelTryOut('POSTapi-provider');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-provider" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/provider</code></b>
</p>
<p>
<label id="auth-POSTapi-provider" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="POSTapi-provider" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="address" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="phone" data-endpoint="POSTapi-provider" data-component="body"  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="POSTapi-provider" data-component="body"  hidden>
<br>
O email do fornecedor. O campo value deve ser um endereço de e-mail válido.
</p>
<p>
<details>
<summary>
<b><code>debts</code></b>&nbsp;&nbsp;<small>object[]</small>     <i>optional</i> &nbsp;
<br>

</summary>
<br>
<p>
<b><code>debts[].description</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="debts.0.description" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
A descrição da conta a pagar.
</p>
<p>
<b><code>debts[].value</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
<input type="number" name="debts.0.value" data-endpoint="POSTapi-provider" data-component="body" required  hidden>
<br>
O valor da conta a pagar.
</p>
</details>
</p>

</form>


## Detalhar Fornecedor

<small class="badge badge-darkred">requires authentication</small>

Retorna os dados do fornecedor

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8000/api/provider/4" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/provider/4"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Provider] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Nome Atualizado 2",
        "phone": "(469) 672-3724",
        "address": "Novo Endereço",
        "email": "harry19@example.com",
        "created_at": "2021-05-01T19:24:55.000000Z",
        "updated_at": "2021-05-01T20:28:19.000000Z",
        "debts": [
            {
                "debt_id": 1,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-08T14:43:35.000000Z",
                "updated_at": "2021-05-09T15:03:36.000000Z"
            },
            {
                "debt_id": 3,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-10T22:38:08.000000Z",
                "updated_at": "2021-05-10T22:38:08.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-GETapi-provider--provider-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-provider--provider-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-provider--provider-"></code></pre>
</div>
<div id="execution-error-GETapi-provider--provider-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-provider--provider-"></code></pre>
</div>
<form id="form-GETapi-provider--provider-" data-method="GET" data-path="api/provider/{provider}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-provider--provider-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-provider--provider-" onclick="tryItOut('GETapi-provider--provider-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-provider--provider-" onclick="cancelTryOut('GETapi-provider--provider-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-provider--provider-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<label id="auth-GETapi-provider--provider-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="GETapi-provider--provider-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>provider</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider" data-endpoint="GETapi-provider--provider-" data-component="url" required  hidden>
<br>
O id do fornecedor
</p>
</form>


## Atualizar Fornecedor

<small class="badge badge-darkred">requires authentication</small>

Atualiza os dados do fornecedor

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/provider/4" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"FORNECEDOR DE ENERGIA","phone":"(38) 4002-8922","address":"Avenida Brasil, 200, S\u00e3o Paulo","email":"energiaecia@fornecedor.com"}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/provider/4"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "FORNECEDOR DE ENERGIA",
    "phone": "(38) 4002-8922",
    "address": "Avenida Brasil, 200, S\u00e3o Paulo",
    "email": "energiaecia@fornecedor.com"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Provider] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "id": 1,
        "name": "Nome Atualizado 2",
        "phone": "(469) 672-3724",
        "address": "Novo Endereço",
        "email": "harry19@example.com",
        "created_at": "2021-05-01T19:24:55.000000Z",
        "updated_at": "2021-05-01T20:28:19.000000Z",
        "debts": [
            {
                "debt_id": 1,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-08T14:43:35.000000Z",
                "updated_at": "2021-05-09T15:03:36.000000Z"
            },
            {
                "debt_id": 3,
                "provider_id": 1,
                "description": "teste teste teste",
                "value": 10.1,
                "payed_at": null,
                "created_at": "2021-05-10T22:38:08.000000Z",
                "updated_at": "2021-05-10T22:38:08.000000Z"
            }
        ]
    }
}
```
> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "name": [
            "O campo name não pode ser superior a 255 caracteres.",
            "O campo name deve ter um valor."
        ],
        "address": [
            "O campo address não pode ser superior a 255 caracteres.",
            "O campo address deve ter um valor."
        ],
        "email": [
            "O campo email deve ser um endereço de e-mail válido.",
            "O campo email não pode ser superior a 20 caracteres.",
            "O campo email deve ter um valor."
        ],
        "phone": [
            "O campo phone não pode ser superior a 20 caracteres.",
            "O campo phone deve ter um valor."
        ]
    }
}
```
<div id="execution-results-PUTapi-provider--provider-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-provider--provider-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-provider--provider-"></code></pre>
</div>
<div id="execution-error-PUTapi-provider--provider-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-provider--provider-"></code></pre>
</div>
<form id="form-PUTapi-provider--provider-" data-method="PUT" data-path="api/provider/{provider}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-provider--provider-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-provider--provider-" onclick="tryItOut('PUTapi-provider--provider-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-provider--provider-" onclick="cancelTryOut('PUTapi-provider--provider-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-provider--provider-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<small class="badge badge-purple">PATCH</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<label id="auth-PUTapi-provider--provider-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-provider--provider-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>provider</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider" data-endpoint="PUTapi-provider--provider-" data-component="url" required  hidden>
<br>
O id do fornecedor
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="name" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O nome do fornecedor.
</p>
<p>
<b><code>phone</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="phone" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O telefone do fornecedor.
</p>
<p>
<b><code>address</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="address" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O endereço do fornecedor.
</p>
<p>
<b><code>email</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="email" data-endpoint="PUTapi-provider--provider-" data-component="body"  hidden>
<br>
O email do fornecedor. O campo value deve ser um endereço de e-mail válido.
</p>

</form>


## Excluir Fornecedor

<small class="badge badge-darkred">requires authentication</small>

Exclui um fornecedor

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8000/api/provider/12" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/provider/12"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Provider] 3"
}
```
<div id="execution-results-DELETEapi-provider--provider-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-provider--provider-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-provider--provider-"></code></pre>
</div>
<div id="execution-error-DELETEapi-provider--provider-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-provider--provider-"></code></pre>
</div>
<form id="form-DELETEapi-provider--provider-" data-method="DELETE" data-path="api/provider/{provider}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-provider--provider-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-provider--provider-" onclick="tryItOut('DELETEapi-provider--provider-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-provider--provider-" onclick="cancelTryOut('DELETEapi-provider--provider-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-provider--provider-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/provider/{provider}</code></b>
</p>
<p>
<label id="auth-DELETEapi-provider--provider-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="DELETEapi-provider--provider-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>provider</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="provider" data-endpoint="DELETEapi-provider--provider-" data-component="url" required  hidden>
<br>
O id do fornecedor
</p>
</form>



