<?php

namespace App\Http\Requests\Debt;

class DebtStore extends DebtDoc
{
    public function authorize()
    {
        return auth()->user()->tokenCan('create');
    }

    public function rules()
    {
        return [
            'provider_id' => ['required', 'integer', 'exists:providers,id'],
            'description' => ['required', 'string', 'max:255'],
            'value'       => ['required', 'numeric'],
        ];
    }
}
