<?php

namespace App\Providers;

use Domain\PDF\Pdf;
use Domain\PDF\PdfInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(PdfInterface::class, Pdf::class);
    }

    public function boot()
    {
        // DB::listen(function ($query) {
        //     Log::info($query->sql);
        //     Log::info($query->bindings);
        //     Log::info($query->time);
        // });
    }
}
