<?php

namespace Domain\DebtPayment;

use App\Models\Debt as DebtModel;
use Illuminate\Support\Facades\DB;

class Refund
{
    public function refund(DebtModel $accountDebt): void
    {
        DB::transaction(function () use ($accountDebt) {
            $accountDebt->payed_at = null;
            $accountDebt->save();

            $accountDebt->history()->create([
                'user_id' => auth()->id(),
                'is_payment' => false
            ]);
        });
    }
}
