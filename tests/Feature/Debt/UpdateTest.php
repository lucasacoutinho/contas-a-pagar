<?php

namespace Tests\Feature\Debt;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;

class UpdateTest extends TestCase
{
    private const ROUTE = 'account-debts.update';

    public function test_success()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $newData = Debt::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['update'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $debt->getKey()), $newData);

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    'debt_id'     => $debt->getKey(),
                    'provider_id' => $newData['provider_id'],
                    'description' => $newData['description'],
                    'value'       => $newData['value'],
                    'payed_at'    => $debt->payed_at,
                    'created_at'  => $debt->created_at,
                    'updated_at'  => $response['data']['updated_at'],
                    'history'     => [],
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $newData = Debt::factory()->make()->toArray();

        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $debt->getKey()), $newData);

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $newData = Debt::factory()->make()->toArray();

        $response = $this->putJson(route(self::ROUTE, $debt->getKey()), $newData);

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
