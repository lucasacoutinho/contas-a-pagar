<?php

namespace App\Http\Requests\BankAccount;

use Illuminate\Foundation\Http\FormRequest;

abstract class BankAccountDoc extends FormRequest
{
    public function bodyParameters()
    {
        return [
            'bank_name' => [
                'description' => 'Nome do Banco',
                'example' => 'ITAU',
            ],
            'agency_number' => [
                'description' => 'Agencia bancaria',
                'example' => 'BRA01',
            ],
            'account_number' => [
                'description' => 'Numero da conta bancaria',
                'example' => '2003427881',
            ],
            'balance_start' => [
                'description' => 'Balanço da conta bancaria',
                'example' => '154491.26',
            ],
        ];
    }
}
