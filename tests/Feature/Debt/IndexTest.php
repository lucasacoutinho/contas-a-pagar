<?php

namespace Tests\Feature\Debt;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;
use App\Models\DebtPaymentHistory;

class IndexTest extends TestCase
{
    private const ROUTE = 'account-debts.index';

    public function test_success()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['read'])->plainTextToken;

        $response = $this->withToken($token)->getJson(route(self::ROUTE));

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    [
                        'debt_id'     => $debt->getKey(),
                        'provider_id' => $debt->provider_id,
                        'description' => $debt->description,
                        'value'       => $debt->value,
                        'payed_at'    => $debt->payed_at,
                        'created_at'  => $debt->created_at,
                        'updated_at'  => $debt->updated_at,
                        'history'     => [],
                    ]
                ]
            ]);
    }

    public function test_success_with_history()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->create();
        $user = User::factory()->create();
        DebtPaymentHistory::factory(3)->create();
        $history = DebtPaymentHistory::with('user')->get();

        $token = $user->createToken('token-name', ['read'])->plainTextToken;

        $response = $this->withToken($token)->getJson(route(self::ROUTE));

        $historyCollection = [];
        foreach ($history as $data) {
            $historyCollection[] = [
                'history_id' => $data->getKey(),
                'user_id'    => $data->user_id,
                'user_name'  => $data->user->name,
                'is_payment' => $data->is_payment,
                'created_at' => $data->created_at,
                'updated_at' => $data->updated_at,
            ];
        }

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    [
                        'debt_id'     => $debt->getKey(),
                        'provider_id' => $debt->provider_id,
                        'description' => $debt->description,
                        'value'       => $debt->value,
                        'payed_at'    => $debt->payed_at,
                        'created_at'  => $debt->created_at,
                        'updated_at'  => $debt->updated_at,
                        'history'     => $historyCollection,
                    ]
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->getJson(route(self::ROUTE));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $response = $this->getJson(route(self::ROUTE));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
