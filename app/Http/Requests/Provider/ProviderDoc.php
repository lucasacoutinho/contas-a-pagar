<?php

namespace App\Http\Requests\Provider;

use Illuminate\Foundation\Http\FormRequest;

abstract class ProviderDoc extends FormRequest
{
    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'O nome do fornecedor.',
                'example' => 'FORNECEDOR DE ENERGIA',
            ],
            'address' => [
                'description' => 'O endereço do fornecedor.',
                'example' => 'Avenida Brasil, 200, São Paulo',
            ],
            'phone' => [
                'description' => 'O telefone do fornecedor.',
                'example' => '(38) 4002-8922',
            ],
            'email' => [
                'description' => 'O email do fornecedor.',
                'example' => 'energiaecia@fornecedor.com',
            ],
            'debts.*.description' => [
                'description' => 'A descrição da conta a pagar.',
                'example' => 'CONTA DE ENERGIA',
            ],
            'debts.*.value' => [
                'description' => 'O valor da conta a pagar.',
                'example' => 99.67,
            ],
        ];
    }
}
