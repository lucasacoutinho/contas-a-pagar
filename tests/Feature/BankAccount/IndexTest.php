<?php

namespace Tests\Feature\BankAccount;

use Tests\TestCase;
use App\Models\User;
use App\Models\BankAccount;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexTest extends TestCase
{
    private const ROUTE = 'bank-account.index';

    public function test_success()
    {
        $account = BankAccount::factory()->create();

        $token = User::factory()->create()->createToken('token-name', ['read'])->plainTextToken;

        $response = $this->withToken($token)->getJson(route(self::ROUTE));

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    [
                        "account_id"     => $account->getKey(),
                        "bank_name"      => $account->bank_name,
                        "agency_number"  => $account->agency_number,
                        "account_number" => $account->account_number,
                        "balance_start"  => $account->balance_start,
                        "created_at"     => $account->created_at,
                        "updated_at"     => $account->updated_at,
                    ]
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        $account = BankAccount::factory()->create();

        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->getJson(route(self::ROUTE));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $response = $this->getJson(route(self::ROUTE));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
