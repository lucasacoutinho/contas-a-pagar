<?php

namespace Tests\Feature\BankAccount;

use Tests\TestCase;
use App\Models\User;
use App\Models\BankAccount;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DestroyTest extends TestCase
{
    private const ROUTE = 'bank-account.destroy';

    public function test_success()
    {
        $account = BankAccount::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['delete'])->plainTextToken;

        $response = $this->withToken($token)->deleteJson(route(self::ROUTE, $account->getKey()));

        $response->assertStatus(200)->assertSee('');
    }

    public function test_failure_prohibited()
    {
        $account = BankAccount::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->deleteJson(route(self::ROUTE, $account->getKey()));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $account = BankAccount::factory()->create();

        $response = $this->deleteJson(route(self::ROUTE, $account->getKey()));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
