# Conta a Pagar Liquidar/Estornar


## Liquidar conta a pagar

<small class="badge badge-darkred">requires authentication</small>

Realiza a liquidação de uma conta a pagar

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/account-debts/pay/9" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/pay/9"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Debt] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-PUTapi-account-debts-pay--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-account-debts-pay--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-account-debts-pay--account_debt-"></code></pre>
</div>
<div id="execution-error-PUTapi-account-debts-pay--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-account-debts-pay--account_debt-"></code></pre>
</div>
<form id="form-PUTapi-account-debts-pay--account_debt-" data-method="PUT" data-path="api/account-debts/pay/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-account-debts-pay--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-account-debts-pay--account_debt-" onclick="tryItOut('PUTapi-account-debts-pay--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-account-debts-pay--account_debt-" onclick="cancelTryOut('PUTapi-account-debts-pay--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-account-debts-pay--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/account-debts/pay/{account_debt}</code></b>
</p>
<p>
<label id="auth-PUTapi-account-debts-pay--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-account-debts-pay--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="PUTapi-account-debts-pay--account_debt-" data-component="url" required  hidden>
<br>
O id da conta a pagar
</p>
</form>


## Estornar valor da transação

<small class="badge badge-darkred">requires authentication</small>

Realiza o estorno de um valor pago

> Example request:

```bash
curl -X PUT \
    "http://127.0.0.1:8000/api/account-debts/refund/20" \
    -H "Authorization: Bearer {YOUR_AUTH_KEY}" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8000/api/account-debts/refund/20"
);

let headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "PUT",
    headers,
}).then(response => response.json());
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Debt] 3"
}
```
> Example response (200):

```json
{
    "data": {
        "debt_id": 11,
        "provider_id": 72,
        "description": "teste teste teste",
        "value": 4012.12,
        "payed_at": null,
        "created_at": "2021-05-13T13:06:44.000000Z",
        "updated_at": "2021-05-13T17:53:32.000000Z",
        "history": [
            {
                "history_id": 6,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": true,
                "created_at": "2021-05-13T17:21:11.000000Z",
                "updated_at": "2021-05-13T17:21:11.000000Z"
            },
            {
                "history_id": 12,
                "user_id": 1,
                "user_name": "Rosalyn Winters",
                "is_payment": false,
                "created_at": "2021-05-13T17:53:32.000000Z",
                "updated_at": "2021-05-13T17:53:32.000000Z"
            }
        ]
    }
}
```
<div id="execution-results-PUTapi-account-debts-refund--account_debt-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-account-debts-refund--account_debt-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-account-debts-refund--account_debt-"></code></pre>
</div>
<div id="execution-error-PUTapi-account-debts-refund--account_debt-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-account-debts-refund--account_debt-"></code></pre>
</div>
<form id="form-PUTapi-account-debts-refund--account_debt-" data-method="PUT" data-path="api/account-debts/refund/{account_debt}" data-authed="1" data-hasfiles="0" data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-account-debts-refund--account_debt-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-account-debts-refund--account_debt-" onclick="tryItOut('PUTapi-account-debts-refund--account_debt-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-account-debts-refund--account_debt-" onclick="cancelTryOut('PUTapi-account-debts-refund--account_debt-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-account-debts-refund--account_debt-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/account-debts/refund/{account_debt}</code></b>
</p>
<p>
<label id="auth-PUTapi-account-debts-refund--account_debt-" hidden>Authorization header: <b><code>Bearer </code></b><input type="text" name="Authorization" data-prefix="Bearer " data-endpoint="PUTapi-account-debts-refund--account_debt-" data-component="header"></label>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>account_debt</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="account_debt" data-endpoint="PUTapi-account-debts-refund--account_debt-" data-component="url" required  hidden>
<br>
O id da conta a pagar
</p>
</form>



