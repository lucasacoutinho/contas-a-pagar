<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtsTable extends Migration
{
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('provider_id')
                  ->constrained('providers')
                  ->onDelete('CASCADE')
                  ->onUpdate('CASCADE');

            $table->string('description');
            $table->decimal('value');
            
            $table->timestamp('payed_at')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('debts');
    }
}
