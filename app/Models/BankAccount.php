<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BankAccount extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'bank_accounts';

    protected $fillable = [
        'bank_name',
        'agency_number',
        'account_number',
        'balance_start',
    ];

    protected $casts = [
        'agency_number' => 'string',
        'account_number' => 'string',
        'balance_start' => 'float',
    ];
}
