<?php

namespace Domain\Provider;

use App\Models\Provider as Model;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Provider\ProviderStore as RequestStore;

class Store
{
    public function store(RequestStore $request) : null|Model
    {
        $new = null;
        DB::transaction(function () use (&$new, $request) {
            $new = Model::create($request->all());
            if ($request->has('debts')) {
                $new->debts()->createMany($request->input('debts'));
            }
        });

        return $new;
    }
}
