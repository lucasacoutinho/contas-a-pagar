<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DebtController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\DebtReportController;
use App\Http\Controllers\BankAccountController;
use App\Http\Controllers\DebtPaymentController;
use App\Http\Controllers\DebtPaymentRefundController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Listagem de usuarios
 *
 * Retorna todos os usuarios cadastrados
 * @responsefile Response/Usuario/Listar.json
 */
Route::middleware('auth:sanctum')->get('/user', function () {
    return User::all();
});

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('/provider', ProviderController::class);
    Route::apiResource('/bank-account', BankAccountController::class);
    Route::apiResource('/account-debts', DebtController::class);
    Route::put('/account-debts/pay/{account_debt}', DebtPaymentController::class)->name('pay');
    Route::put('/account-debts/refund/{account_debt}', DebtPaymentRefundController::class)->name('refund');
    Route::get('/account-debts/report/pending', [DebtReportController::class, 'pending'])->name('report.pending');
    Route::get('/account-debts/report/payed', [DebtReportController::class, 'payed'])->name('report.payed');
});
