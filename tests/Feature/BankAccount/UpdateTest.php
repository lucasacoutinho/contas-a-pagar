<?php

namespace Tests\Feature\BankAccount;

use Tests\TestCase;
use App\Models\User;
use App\Models\BankAccount;

class UpdateTest extends TestCase
{
    private const ROUTE = 'bank-account.update';

    public function test_success()
    {
        $account = BankAccount::factory()->create();
        $newData = BankAccount::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['update'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $account->getKey()), $newData);

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    "account_id"     => $account->getKey(),
                    "bank_name"      => $newData['bank_name'],
                    "agency_number"  => $newData['agency_number'],
                    "account_number" => $newData['account_number'],
                    "balance_start"  => $newData['balance_start'],
                    "created_at"     => $account->created_at,
                    "updated_at"     => $account->updated_at,
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        $account = BankAccount::factory()->create();
        $newData = BankAccount::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $account->getKey()), $newData);

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $account = BankAccount::factory()->create();
        $newData = BankAccount::factory()->make()->toArray();

        $response = $this->putJson(route(self::ROUTE, $account->getKey()), $newData);

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
