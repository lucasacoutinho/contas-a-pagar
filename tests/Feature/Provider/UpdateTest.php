<?php

namespace Tests\Feature\Provider;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;
use App\Http\Resources\DebtPaymentHistory\History;

class UpdateTest extends TestCase
{
    private const ROUTE = 'provider.update';

    public function test_success()
    {
        $provider = Provider::factory()->create();
        $newData = Provider::factory()->make()->toArray();
        $token = User::factory()->create()->createToken('token-name', ['update'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $provider->getKey()), $newData);

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    "id"         => $provider->getKey(),
                    "name"       => $newData['name'],
                    "phone"      => $newData['phone'],
                    "address"    => $newData['address'],
                    "email"      => $newData['email'],
                    "created_at" => $provider->created_at,
                    "updated_at" => $response['data']['updated_at'],
                    "debts"      => [],
                ]
            ]);
    }

    public function test_success_with_debts()
    {
        $provider = Provider::factory()->create();
        $newData = Provider::factory()->make()->toArray();
        $debts = Debt::factory(10)->create();
        $token = User::factory()->create()->createToken('token-name', ['update'])->plainTextToken;

        $debtsCollection = [];
        foreach ($debts as $data) {
            $debtsCollection[] = [
                'debt_id'     => $data->id,
                'provider_id' => $data->provider_id,
                'description' => $data->description,
                'value'       => $data->value,
                'payed_at'    => $data->payed_at,
                'created_at'  => $data->created_at,
                'updated_at'  => $data->updated_at,
                'history'     => History::collection($data->history)
            ];
        }

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $provider->getKey()), $newData);

        $response->assertStatus(200)
            ->assertJsonFragment([
                "data" => [
                    "id"         => $provider->getKey(),
                    "name"       => $newData['name'],
                    "phone"      => $newData['phone'],
                    "address"    => $newData['address'],
                    "email"      => $newData['email'],
                    "created_at" => $provider->created_at,
                    "updated_at" => $response['data']['updated_at'],
                    "debts"      => $debtsCollection,
                ]
            ]);
    }

    public function test_failure_prohibited()
    {
        $provider = Provider::factory()->create();
        $newData = Provider::factory()->make()->toArray();

        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->putJson(route(self::ROUTE, $provider->getKey()), $newData);

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $provider = Provider::factory()->create();
        $newData = Provider::factory()->make()->toArray();

        $response = $this->putJson(route(self::ROUTE, $provider->getKey()), $newData);

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
