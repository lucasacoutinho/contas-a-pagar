<?php

namespace App\Http\Requests\Debt;

use Illuminate\Foundation\Http\FormRequest;

abstract class DebtDoc extends FormRequest
{
    public function bodyParameters()
    {
        return [
            'provider_id' => [
                'description' => 'id do fornecedor ao qual se tem a divida',
                'example' => 1234
            ],
            'description' => [
                'description' => 'Descrição da divida',
                'example' => 'CONTA DE LUZ'
            ],
            'value' => [
                'description' => 'Valor da divida',
                'example' => 10101.12
            ],
        ];
    }
}
