<?php

namespace Tests\Feature\Reports;

use Tests\TestCase;
use App\Models\Debt;
use App\Models\User;
use App\Models\Provider;

class DebtsPayedTest extends TestCase
{
    private const ROUTE = 'report.payed';

    public function test_success()
    {
        Provider::factory()->create();
        $debt = Debt::factory()->payed()->create();
        $token = User::factory()->create()->createToken('token-name', ['report'])->plainTextToken;

        $response = $this->withToken($token)->getJson(route(self::ROUTE));

        $response->assertStatus(200)
            ->assertJsonFragment([[
                'id'          => $debt->getKey(),
                'provider_id' => $debt->provider_id,
                'description' => $debt->description,
                'value'       => $debt->value,
                'payed_at'    => $debt->payed_at,
                'created_at'  => $debt->created_at,
                'updated_at'  => $debt->updated_at,
            ]]);
    }

    public function test_failure_prohibited()
    {
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->getJson(route(self::ROUTE));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $response = $this->getJson(route(self::ROUTE));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
