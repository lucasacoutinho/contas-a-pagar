<?php

namespace Tests\Feature\Provider;

use Tests\TestCase;
use App\Models\User;
use App\Models\Provider;

class DestroyTest extends TestCase
{
    private const ROUTE = 'provider.destroy';

    public function test_success()
    {
        $provider = Provider::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['delete'])->plainTextToken;

        $response = $this->withToken($token)->deleteJson(route(self::ROUTE, $provider->getKey()));

        $response->assertStatus(200)->assertSee('');
    }

    public function test_failure_prohibited()
    {
        $provider = Provider::factory()->create();
        $token = User::factory()->create()->createToken('token-name', ['no-permission'])->plainTextToken;

        $response = $this->withToken($token)->deleteJson(route(self::ROUTE, $provider->getKey()));

        $response->assertStatus(403)->assertJsonFragment(["message" => "This action is unauthorized."]);
    }

    public function test_failure_no_token()
    {
        $provider = Provider::factory()->create();

        $response = $this->deleteJson(route(self::ROUTE, $provider->getKey()));

        $response->assertStatus(401)->assertJsonFragment(["message" => "Unauthenticated."]);
    }
}
