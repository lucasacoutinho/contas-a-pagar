<?php

namespace Database\Factories;

use App\Models\Debt;
use App\Models\User;
use App\Models\DebtPaymentHistory;
use Illuminate\Database\Eloquent\Factories\Factory;

class DebtPaymentHistoryFactory extends Factory
{
    protected $model = DebtPaymentHistory::class;

    public function definition()
    {
        return [
            'user_id' => User::inRandomOrder()->first()->getKey(),
            'debt_id' => Debt::inRandomOrder()->first()->getKey(),
            'is_payment' => false,
        ];
    }
}
